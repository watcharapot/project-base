from django.conf.urls import url
from . import views
from Report.views import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path

app_name = 'Report'
# set name url with views report application
urlpatterns = [
    path('pdf/', PDF.as_view(),name='pdf'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
