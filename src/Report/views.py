from __future__ import unicode_literals
from django.template.loader import get_template, render_to_string
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse
from DigitalBiobank.utils import render_to_pdf
from MainApp.helper import *
from django.conf import settings
from django.contrib.auth.decorators import login_required
import shutil
import re
import numpy as np
import pandas as pd
import os.path, time
from datetime import datetime
from operator import itemgetter
from datetime import timedelta
from weasyprint import HTML, CSS
from django.conf import settings
import base64
from PIL import Image
from io import BytesIO

# Create your views here
path_static = 'fasta_files/Processeddata/'

# function get template and render page pdf
class PDF(View):
    def post(self, request, *args, **kwargs):
        domain = request.META['HTTP_HOST']
        uid = str(request.user.id)

        context = {
            'welcome':'Hello'
        }

        html_string = render_to_string('pdf.html', context)

        html = HTML(string=html_string, encoding="utf-8")

        pdf = html.write_pdf(stylesheets=[
            CSS(settings.STATICFILES_DIRS[1] + '/UI/css/sb-admin-2.min.css'),
            CSS('https://fonts.googleapis.com/css2?family=Sarabun:wght@200;300;400;500;600;700&display=swap"'),
            CSS(string='body { font-family: "Sarabun", sans-serif; }'),
        ])

        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename="pdf.pdf"'

        return response
        
    def get(self, request, *args, **kwargs):
        domain = request.META['HTTP_HOST']
        uid = str(request.user.id)

        context = {
            'welcome':'Hello'
        }

        html_string = render_to_string('pdf.html', context)

        html = HTML(string=html_string, encoding="utf-8")

        pdf = html.write_pdf(stylesheets=[
            CSS(settings.STATICFILES_DIRS[1] + '/UI/css/sb-admin-2.min.css'),
            CSS('https://fonts.googleapis.com/css2?family=Sarabun:wght@200;300;400;500;600;700&display=swap"'),
            CSS(string='body { font-family: "Sarabun", sans-serif; }'),
        ])

        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename="pdf.pdf"'

        return response



def copy_image(pid,uid):
    minioClient = connect_minio()
    for path, dirs, files in os.walk(settings.MEDIA_ROOT+'/storage/'+uid+'/'+pid+'/img'):
        # print (path)
        paths = path.split('/')
        for n,f in enumerate(files):
            # to_minio = '/'.join(paths[8:])+'/'+f
            to_minio = uid+'/'+pid+'/img/'+f
            from_dev = path+'/'+f
            # print('------------------------')
            # print(from_dev)
            # print(to_minio)
            # print('------------------------')
            minioClient.fput_object(settings.MINIO_BUCKET_NAME, to_minio , from_dev)

    shutil.rmtree(settings.MEDIA_ROOT+'/storage/'+uid+'/'+pid+'/img')
    



# # function get template and render page pdf
# class PDF(View):
#     def post(self, request, *args, **kwargs):
#         domain = request.META['HTTP_HOST']
#         user_id = str(request.user.id)
#         # DB
#         context = {

#         }

#         pdf = render_to_pdf("pdf.html", context)
#         return HttpResponse(pdf, content_type= "application/pdf")