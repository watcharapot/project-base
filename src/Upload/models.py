from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User

# Create your models here.
class FileUpload(models.Model):
    # pk
    id = models.AutoField(primary_key=True)
    # info
    file_name = models.CharField(max_length=255)
    path = models.TextField(max_length=500)
    size = models.CharField(max_length=255)
    datetime_latest = models.DateTimeField(default=now, editable=False, blank=True)
    # fk
    user = models.ForeignKey(User,on_delete=models.CASCADE)