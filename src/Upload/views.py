from __future__ import unicode_literals
from django.http import HttpResponse,JsonResponse
from django.template import loader
from django.db import connection
from django.template.loader import get_template
from django.template import Context
import subprocess
import shlex
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from ManageUsers.models import user_task
import base64
import json
from django.db import connections
from MainApp.helper import *
from ratelimit.decorators import ratelimit
from django.conf import settings
# celery
# from .task import *
from celery.result import AsyncResult
from time import sleep
from datetime import datetime
from django.core.mail import EmailMessage
from datetime import datetime, timedelta
import binascii


# json in array for row
def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


# Home page
@ratelimit(key='ip', rate='30/1m', block=True)
@login_required(login_url="/home/")
def upload_file(request,project_id):
        
    enpid = project_id
    uid = str(request.user.id)
    
    statistic_table = 'None'

    content = {
        'pid':enpid,
        'uid':uid,
        'statistic_table': statistic_table,
    }
 
    return render(request, 'upload_page.html', content)


@ratelimit(key='ip', rate='200/1m', block=True)
def minio_list_file_input(request):
    pid = decode_url(request.GET.get('pid',False))
    uid = decode_url(request.GET.get('uid',False))
    myfile = []

    size = 0
    if  uid and pid:
        minioClient = connect_minio()
        objects = minioClient.list_objects(settings.MINIO_BUCKET_NAME, prefix=uid+'/'+pid+'/input/',recursive=True)
        for obj in objects:
            myfile.append([
                (obj.object_name).split('/')[-1],
                obj.size/1024/1024
            ])
            size += obj.size/1024/1024
    count = len(myfile)

    return JsonResponse({'myfile':myfile,'size':size,'count':count})


@ratelimit(key='ip', rate='200/1m', block=True)
def minio_remove_file(request):
    name = request.GET.get('name',False)
    pid = decode_url(request.GET.get('pid',False))
    uid = decode_url(request.GET.get('uid',False))
    
    try:
        if  uid and pid:
            minioClient = connect_minio()
            minioClient.remove_object(settings.MINIO_BUCKET_NAME, uid+'/'+pid+'/input/'+name)
        
        return JsonResponse({'result':True})
    
    except ResponseError as err:
        
        return JsonResponse({'result':False})

    return JsonResponse({'result':False})


@ratelimit(key='ip', rate='200/1m', block=True)
def minio_presigned_url(request):
    name = request.GET.get('name',False)
    pid = decode_url(request.GET.get('pid',False))
    uid = decode_url(request.GET.get('uid',False))
    urll = None
    name = str(name).replace('-','').replace('_','').replace(' ','').replace('(','').replace(')','').replace('/','')
    if name and uid and pid:
        minioClient = connect_minio()
        urll = minioClient.presigned_put_object(settings.MINIO_BUCKET_NAME, uid+'/'+pid+'/input/'+name , expires=timedelta(hours=2))


    resp = JsonResponse({'urll':urll})
    resp['Access-Control-Allow-Origin'] = '*'
    return resp  


def upload_file_by_url(request):
    status = 'None'
    if request.method == 'POST':
        list_file = request.FILES.getlist("file_upload[]")
        for count, x in enumerate(list_file):
            def process(file, filename):
                media = settings.MEDIA_ROOT
 
                with open(media+"/storage/"+filename, 'wb+') as destination:
                    for chunk in file.chunks():
                        destination.write(chunk)
                
                return str(filename)

            process(x,x)
            status = 'ok'
        
        content = {
            'status':status
        }
    return JsonResponse(content)