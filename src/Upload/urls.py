from django.conf.urls import url
from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth.decorators import login_required
from django.conf.urls.static import static
from django.conf import settings
from . import views
from django.views.static import serve
from django.conf import settings
from MainApp.helper import decode_url
from django.shortcuts import redirect

app_name = 'upload'

urlpatterns = [
    path('upload/<str:project_id>/', views.upload_file, name='upload'),
    path('presigned_upload/', views.minio_presigned_url, name='presigned_upload'),
    path('list_my_file/', views.minio_list_file_input ,name='list_my_file'),
    path('remove_my_file/', views.minio_remove_file ,name='remove_my_file'),
    path('local_upload/', views.upload_file_by_url ,name='local_upload'),


    # url(r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], protected_serve, {'document_root': settings.MEDIA_ROOT}),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
