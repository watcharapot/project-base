#!/bin/bash


source /app/src/.env && python3 /app/src/manage.py migrate
source /app/src/.env && python3 /app/src/manage.py loaddata /app/group.json
source /app/src/.env && python3 /app/src/manage.py loaddata /app/user.json
source /app/src/.env && python3 /app/src/manage.py loaddata /app/profile.json
source /app/src/.env && python3 /app/src/manage.py loaddata /app/user_groups.json
source /app/src/.env && python3 /app/src/manage.py runserver 0:8000

# source $PWD/.env && python3 $PWD/manage.py migrate
# source $PWD/.env && python3 $PWD/manage.py loaddata $PWD/group.json
# source $PWD/.env && python3 $PWD/manage.py loaddata $PWD/user.json
# source $PWD/.env && python3 $PWD/manage.py loaddata $PWD/profile.json
# source $PWD/.env && python3 $PWD/manage.py loaddata $PWD/user_groups.json
# source $PWD/.env && python3 $PWD/manage.py runserver 0:8000
