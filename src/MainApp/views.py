from __future__ import unicode_literals
from django.http import HttpResponse,JsonResponse
from django.template import loader
from django.db import connection
from django.template.loader import get_template
from django.template import Context
import subprocess
import shlex
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from ManageUsers.models import user_task
import base64
import json
from django.db import connections
from MainApp.helper import *
from ratelimit.decorators import ratelimit
from django.conf import settings
from pytz import timezone
# celery
from .task import *
from celery.result import AsyncResult
from time import sleep
from django.core.mail import EmailMessage
from datetime import datetime, timedelta
import binascii
from .view_run import run
import pandas as pd
import numpy as np
from django.contrib.auth import login as dj_login, logout as dj_logout
from ManageUsers.forms import AdminFormSignup, LoginForm , UserFormSignup , ResetChangePasswordForm, ChangePasswordForm ,PasswordResetRequestForm



# home page for user
homePath = '/home/'
dashboardPath = '/dashboard/'
# other page
projectPath = '/ManageUsers'
loginPath = '/home/'
adminPage = '/ManageUsers/AdminApproved/'


# json in array for row
def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


# Home page
@ratelimit(key='ip', rate='30/1m', block=True)
@login_required(login_url="/home/")
def dashboard(request):
    uid = str(request.user.id)
    print('-----------')

    content = {
        'uid':uid
    }
    return render(request, 'dashboard_page.html', content)


@ratelimit(key='ip', rate='30/1m', block=True)
def index(request):
    args = {}
    form = LoginForm(request.POST or None)

    # check if post button login check user pass
    if request.method == 'POST':
        if 'loginBtn' in request.POST and form.is_valid():

            user = form.login(request)
            request.user.id = user.id
            user_id = request.user.id

            cursor = connection.cursor()
            cursor.execute("SELECT auth_group.name as group_name \
                            FROM auth_group , auth_user_groups \
                            WHERE auth_group.id = auth_user_groups.group_id \
                            AND auth_user_groups.user_id = \'"+ str(user_id) +"\'")
            row = dictfetchall(cursor)
            for i in row:
                group_name = str(i["group_name"])
            
            if user is not None:
                if group_name == 'admin':
                    dj_login(request, user)
                    if 'next' in request.POST:
                        return redirect(adminPage)
                    return redirect(adminPage)
                elif group_name == 'user':
                    dj_login(request, user)
                    if 'next' in request.POST:
                        next_to = request.GET.get('next')
                        return redirect(next_to)
                    return redirect(dashboardPath)

                
                return redirect('/ManageUser/logout')

        elif 'test' in request.POST:
            message_header =_('Register successfully.')
            message_detail =_('Please confirm your email.')
            message_header_json = str(message_header)
            message_detail_json = str(message_detail)
            request.session['sm_value'] = {'message_header': message_header_json,
                                           'message_detail': message_detail_json,
                                           'type': 'success',
                                           'displaytime': '10'}
            request.session['goto'] = homePath


            return redirect(projectPath + '/status_message/')

    args['form'] = form

    if request.user.is_authenticated:
        user_id = request.user.id
        cursor = connection.cursor()
        cursor.execute("SELECT auth_group.name as group_name \
                        FROM auth_group , auth_user_groups \
                        WHERE auth_group.id = auth_user_groups.group_id \
                        AND auth_user_groups.user_id = \'"+ str(user_id) +"\'")
        row = dictfetchall(cursor)
        for i in row:
            group_name = str(i["group_name"])

        if group_name == 'user':
            return redirect(homePath)
            # return HttpResponse('user_page')
        elif group_name == 'admin':
            return redirect(adminPage)
            # return HttpResponse('user_page')
        else:
            return redirect('/ManageUsers/logout')

    return render(request, 'home_page.html', args)

# create dir in media/storage/
def check_create_subdir(user_access):
    try:
        if not os.path.exists(settings.MEDIA_ROOT+"/storage/"+user_access):
            path_create = os.path.abspath(settings.MEDIA_ROOT+"/storage/"+user_access)
            os.makedirs(path_create)

    except OSError as e:
        if e.errno == 17:
            return False


# About page
@ratelimit(key='ip', rate='200/1m', block=True)
def about(request):
    try:
        return render(request, 'about_page.html')
    finally:
        print('error render about page')


# help page
@ratelimit(key='ip', rate='200/1m', block=True)
def help(request):
    try:
        return render(request, 'help_page.html')
    finally:
        print('error render help page')


# Contact page
@ratelimit(key='ip', rate='200/1m', block=True)
def contact(request):
    try:
        return render(request, 'contact_page.html')
    finally:
        print('error render contact page')


def celery_email_ncbi():
    celery_id = run_celery.delay(str(datetime.now()))

    return HttpResponse(celery_id)


def run_celery_email_ncbi(request):
    celery_id = celery_email_ncbi()
    return HttpResponse(celery_id)


@ratelimit(key='ip', rate='200/1m', block=True)
def check_celery_id(request,celery_id):
    res = AsyncResult(str(celery_id),app=run_celery)
    return HttpResponse(res.state)


def upload_file(file_upload, access_token):
    myfile = file_upload
    for x in myfile:
        def process(file, filename):
            with open(settings.MEDIA_ROOT+'/storage/'+access_token+'/'+ 'input.txt', 'wb+') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)    
            return str(filename)

        process(x,x)   