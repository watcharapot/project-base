# email ncbi
import email
import imaplib,configparser
import os
import html2text
import time
import poplib
import string, random
from io import StringIO
import traceback,sys
from urllib.request import urlopen
from bs4 import BeautifulSoup
from xml.etree.ElementTree import parse
from html.parser import HTMLParser


def sra_api(SRP_ID):
    print('innnnnnnnnnnnnnnnn')
    var_url = urlopen('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=sra&term='+SRP_ID)
    xmldoc = parse(var_url).getroot()
    
    Id = None
    for item in xmldoc.findall('IdList/Id'):
        Id = item.text
        break
    
    var_url = urlopen('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=sra&id='+Id)
    xmldoc = parse(var_url).getroot()
    
    ExpXml = None
    for item in xmldoc.findall('DocSum/Item'):
        if item.get('Name') == 'ExpXml':
            ExpXml = BeautifulSoup(item.text, "lxml")
    
    study_result = ExpXml.html.body.find('study').get('name')

    return study_result


def find_srp_id_in_email(request):
    param = Ncbi_Mail.objects.filter(id=1).values()
    email_from_ncbi = param[0]['email_from_ncbi']
    email_username = param[0]['email']
    email_password = param[0]['password']
    # --------------------------------------
    email_username = 'biogwas2561@gmail.com'
    email_password = 'BioGwas2018a'

    mail = imaplib.IMAP4_SSL('imap.gmail.com')
    mail.login(email_username, email_password)
    mail.list()
    # Out: list of "folders" aka labels in gmail.
    mail.select("inbox") # connect to inbox.

    result, data = mail.search(None, '(FROM "'+email_from_ncbi+'")' , '(UNSEEN)')
 
    ids = data[0] # data is a list.
    id_list = ids.split() # ids is a space separated string
    
    if len(id_list) > 0:
        for email_id in id_list:
            # print(email_id)
            result, data = mail.fetch(email_id, "(RFC822)") # fetch the email body (RFC822) for the given ID

            raw_email = data[0][1].decode("UTF-8") # here's the body, which is raw text of the whole email

            email_message = email.message_from_string(raw_email)
            for part in email_message.walk():
                if (part.get_content_type() == 'text/plain') and (part.get('Content-Disposition') is None):
                    rows = part.get_payload().split('\r')
                    for i in rows:
                        if 'publications' in i:
                            i = i.split(':')
                            SRP_ID = i[-1].replace('.','').replace(' ','')
                            sra_api(SRP_ID)

    return HttpResponse('ok')