-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 25, 2019 at 09:35 AM
-- Server version: 10.4.4-MariaDB
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `metagenomics_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_group`
--

INSERT INTO `auth_group` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add user_profile', 7, 'add_user_profile'),
(26, 'Can change user_profile', 7, 'change_user_profile'),
(27, 'Can delete user_profile', 7, 'delete_user_profile'),
(28, 'Can view user_profile', 7, 'view_user_profile'),
(29, 'Can add project', 8, 'add_project'),
(30, 'Can change project', 8, 'change_project'),
(31, 'Can delete project', 8, 'delete_project'),
(32, 'Can view project', 8, 'view_project'),
(33, 'Can add share', 9, 'add_share'),
(34, 'Can change share', 9, 'change_share'),
(35, 'Can delete share', 9, 'delete_share'),
(36, 'Can view share', 9, 'view_share'),
(37, 'Can add status', 10, 'add_status'),
(38, 'Can change status', 10, 'change_status'),
(39, 'Can delete status', 10, 'delete_status'),
(40, 'Can view status', 10, 'view_status'),
(41, 'Can add dataset', 11, 'add_dataset'),
(42, 'Can change dataset', 11, 'change_dataset'),
(43, 'Can delete dataset', 11, 'delete_dataset'),
(44, 'Can view dataset', 11, 'view_dataset'),
(45, 'Can add file upload', 12, 'add_fileupload'),
(46, 'Can change file upload', 12, 'change_fileupload'),
(47, 'Can delete file upload', 12, 'delete_fileupload'),
(48, 'Can view file upload', 12, 'view_fileupload'),
(49, 'Can add dataset_ file upload', 13, 'add_dataset_fileupload'),
(50, 'Can change dataset_ file upload', 13, 'change_dataset_fileupload'),
(51, 'Can delete dataset_ file upload', 13, 'delete_dataset_fileupload'),
(52, 'Can view dataset_ file upload', 13, 'view_dataset_fileupload'),
(53, 'Can add mothur_s1_preprocess', 14, 'add_mothur_s1_preprocess'),
(54, 'Can change mothur_s1_preprocess', 14, 'change_mothur_s1_preprocess'),
(55, 'Can delete mothur_s1_preprocess', 14, 'delete_mothur_s1_preprocess'),
(56, 'Can view mothur_s1_preprocess', 14, 'view_mothur_s1_preprocess'),
(57, 'Can add mothur_s2_ prepare', 15, 'add_mothur_s2_prepare'),
(58, 'Can change mothur_s2_ prepare', 15, 'change_mothur_s2_prepare'),
(59, 'Can delete mothur_s2_ prepare', 15, 'delete_mothur_s2_prepare'),
(60, 'Can view mothur_s2_ prepare', 15, 'view_mothur_s2_prepare'),
(61, 'Can add mothur_s3_ prepare', 16, 'add_mothur_s3_prepare'),
(62, 'Can change mothur_s3_ prepare', 16, 'change_mothur_s3_prepare'),
(63, 'Can delete mothur_s3_ prepare', 16, 'delete_mothur_s3_prepare'),
(64, 'Can view mothur_s3_ prepare', 16, 'view_mothur_s3_prepare'),
(65, 'Can add mothur_s3_analysis', 16, 'add_mothur_s3_analysis'),
(66, 'Can change mothur_s3_analysis', 16, 'change_mothur_s3_analysis'),
(67, 'Can delete mothur_s3_analysis', 16, 'delete_mothur_s3_analysis'),
(68, 'Can view mothur_s3_analysis', 16, 'view_mothur_s3_analysis'),
(69, 'Can add qiime1_s1_process', 17, 'add_qiime1_s1_process'),
(70, 'Can change qiime1_s1_process', 17, 'change_qiime1_s1_process'),
(71, 'Can delete qiime1_s1_process', 17, 'delete_qiime1_s1_process'),
(72, 'Can view qiime1_s1_process', 17, 'view_qiime1_s1_process'),
(73, 'Can add qiiem2_s2_prepare_data', 18, 'add_qiiem2_s2_prepare_data'),
(74, 'Can change qiiem2_s2_prepare_data', 18, 'change_qiiem2_s2_prepare_data'),
(75, 'Can delete qiiem2_s2_prepare_data', 18, 'delete_qiiem2_s2_prepare_data'),
(76, 'Can view qiiem2_s2_prepare_data', 18, 'view_qiiem2_s2_prepare_data'),
(77, 'Can add qiiem2_s1_preprocess', 19, 'add_qiiem2_s1_preprocess'),
(78, 'Can change qiiem2_s1_preprocess', 19, 'change_qiiem2_s1_preprocess'),
(79, 'Can delete qiiem2_s1_preprocess', 19, 'delete_qiiem2_s1_preprocess'),
(80, 'Can view qiiem2_s1_preprocess', 19, 'view_qiiem2_s1_preprocess'),
(81, 'Can add mothur_qiime1_s1_preprocess', 20, 'add_mothur_qiime1_s1_preprocess'),
(82, 'Can change mothur_qiime1_s1_preprocess', 20, 'change_mothur_qiime1_s1_preprocess'),
(83, 'Can delete mothur_qiime1_s1_preprocess', 20, 'delete_mothur_qiime1_s1_preprocess'),
(84, 'Can view mothur_qiime1_s1_preprocess', 20, 'view_mothur_qiime1_s1_preprocess'),
(85, 'Can add mothur_qiime1_s2_pick', 21, 'add_mothur_qiime1_s2_pick'),
(86, 'Can change mothur_qiime1_s2_pick', 21, 'change_mothur_qiime1_s2_pick'),
(87, 'Can delete mothur_qiime1_s2_pick', 21, 'delete_mothur_qiime1_s2_pick'),
(88, 'Can view mothur_qiime1_s2_pick', 21, 'view_mothur_qiime1_s2_pick');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(2, 'pbkdf2_sha256$120000$8h4lGrZrHlFp$87IiVlkCzU0kqLfPdBcCCPNxWLlWNUkp99lPrIzHhwE=', '2019-10-18 06:58:34.003476', 0, 'admin@gmail.com', 'Watcharapot', 'Janpoung', 'jaabxnewjr@gmail.com', 0, 1, '2019-03-11 03:25:09.000000');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_user_groups`
--

INSERT INTO `auth_user_groups` (`id`, `user_id`, `group_id`) VALUES
(2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(7, 'ManageUsers', 'user_profile'),
(11, 'Project', 'dataset'),
(13, 'Project', 'dataset_fileupload'),
(12, 'Project', 'fileupload'),
(8, 'Project', 'project'),
(9, 'Project', 'share'),
(10, 'Project', 'status'),
(20, 'RunPipeline', 'mothur_qiime1_s1_preprocess'),
(21, 'RunPipeline', 'mothur_qiime1_s2_pick'),
(14, 'RunPipeline', 'mothur_s1_preprocess'),
(15, 'RunPipeline', 'mothur_s2_prepare'),
(16, 'RunPipeline', 'mothur_s3_analysis'),
(19, 'RunPipeline', 'qiiem2_s1_preprocess'),
(18, 'RunPipeline', 'qiiem2_s2_prepare_data'),
(17, 'RunPipeline', 'qiime1_s1_process'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE IF NOT EXISTS `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-08-02 07:08:44.520981'),
(2, 'auth', '0001_initial', '2019-08-02 07:08:49.680106'),
(3, 'ManageUsers', '0001_initial', '2019-08-02 07:08:50.365125'),
(4, 'Project', '0001_initial', '2019-08-02 07:08:52.406530'),
(5, 'admin', '0001_initial', '2019-08-02 07:08:53.603871'),
(6, 'admin', '0002_logentry_remove_auto_add', '2019-08-02 07:08:53.642338'),
(7, 'admin', '0003_logentry_add_action_flag_choices', '2019-08-02 07:08:53.675838'),
(8, 'contenttypes', '0002_remove_content_type_name', '2019-08-02 07:08:54.122217'),
(9, 'auth', '0002_alter_permission_name_max_length', '2019-08-02 07:08:54.698485'),
(10, 'auth', '0003_alter_user_email_max_length', '2019-08-02 07:08:55.225309'),
(11, 'auth', '0004_alter_user_username_opts', '2019-08-02 07:08:55.251687'),
(12, 'auth', '0005_alter_user_last_login_null', '2019-08-02 07:08:55.560665'),
(13, 'auth', '0006_require_contenttypes_0002', '2019-08-02 07:08:55.619153'),
(14, 'auth', '0007_alter_validators_add_error_messages', '2019-08-02 07:08:55.670838'),
(15, 'auth', '0008_alter_user_username_max_length', '2019-08-02 07:08:55.778124'),
(16, 'auth', '0009_alter_user_last_name_max_length', '2019-08-02 07:08:55.844552'),
(17, 'sessions', '0001_initial', '2019-08-02 07:08:56.187177'),
(18, 'Project', '0002_auto_20190809_1439', '2019-08-09 07:40:05.188252'),
(19, 'Project', '0003_auto_20190809_1607', '2019-08-09 09:07:56.931457'),
(20, 'Project', '0004_auto_20190809_1607', '2019-08-09 09:08:00.803369'),
(21, 'Project', '0002_auto_20190814_0957', '2019-08-14 02:57:54.167661'),
(22, 'Project', '0003_auto_20190814_0957', '2019-08-14 02:57:58.437953'),
(23, 'RunPipeline', '0001_initial', '2019-08-29 08:19:57.224428'),
(24, 'RunPipeline', '0002_auto_20190829_1617', '2019-08-29 09:18:04.462973'),
(25, 'RunPipeline', '0003_qiime1_s1_process', '2019-08-29 10:11:02.468049'),
(26, 'RunPipeline', '0004_qiiem2_s1_preprocess_qiiem2_s2_prepare_data', '2019-08-29 10:25:43.918397'),
(27, 'RunPipeline', '0005_mothur_qiime1_s1_preprocess_mothur_qiime1_s2_pick', '2019-08-29 10:45:14.332340'),
(28, 'RunPipeline', '0006_auto_20190910_1110', '2019-09-10 04:11:20.471329'),
(29, 'Project', '0004_fileupload_datetime', '2019-09-26 03:37:46.151803'),
(30, 'Project', '0005_status_generate_report', '2019-10-22 08:09:42.647545');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('19fvqlx90ly9qtijaz8h2y57atfndgzu', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-09-03 04:00:55.170076'),
('3bbpk24ikm89t8s8c083zjk6rt7mhkle', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-08-20 03:34:15.429521'),
('544tv3hs3zt4lxyrmcry0652sagclehr', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-10-08 03:04:16.691194'),
('6hbr1lulwkjtduv09bllxafhztatf4tc', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-10-17 08:05:52.988705'),
('6rg2zikos2bb6o9pnlrcl6k6orpyi6n3', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-10-09 03:08:21.530106'),
('7hjtepmvo6dw4o62dwi3tkvqntehl68t', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-10-18 09:15:59.945866'),
('7rccz2uk4waokbt3uj9946n74iavez5o', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-09-10 04:21:13.499890'),
('a4i4hb2lbo1asg2ywz5uiy91d9x9gd55', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-10-03 04:18:05.152034'),
('dt62eevyfrxk9giipnjcw4xde3hsqwis', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-09-17 08:11:01.896450'),
('fky7kue8p284j8o5bsyekc3ticwas0ka', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-10-07 08:46:31.328598'),
('hlb5qadvaqawv6e3vk41xbgxu0kgiik3', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-10-29 07:21:16.409220'),
('hxg4rwqvswxgedrxixt7dap8cuzepmp2', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-09-20 05:23:23.655365'),
('i0704m56355bujxbebzi4gh0untt9utt', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-09-27 02:44:40.478915'),
('i8muqt1hphpc66yd7hybvm87yxkk5ay3', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-08-16 08:10:36.666390'),
('j8poy9qoay5u332qbbbtlwda668svfzy', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-10-04 07:16:57.741536'),
('kt6k2iugsc5a35no0ocinb28mt23b8l8', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-08-29 07:42:19.634501'),
('qkjogvzoake6aa20r5nlb6zpn8ufap0h', 'ZWJiMTg2MTMwYzY3YTE5YmJkNGU1NmRlOTM2YTU0YTQ0NGEyNTIyNDp7InVzZXJfaWQiOjJ9', '2019-09-03 03:58:24.715032'),
('rfoz4s3vfcp4o30ne8h9lmper9ouxws2', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-09-20 05:08:07.160358'),
('rwafkkslrq76zxnafbfluc8rner671le', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-09-20 05:07:17.723366'),
('tqdk4o0wd4jvjjjup0imlrucrzrb5ga8', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-11-01 06:58:34.144517'),
('vnzo2ya453162olol20i5l9p3k6m56hg', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-10-15 04:19:03.394582'),
('vwpiatm5ysny3dx4q1evl11q8xi096g5', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-08-29 07:31:49.049316'),
('vy93qxawhm4ykivs62sole2gksqc87s7', 'NWUxYzk3ZGM2NDhlMThjODAzYWQ3NzI1MDg4MDk1MzlmNTk5YjZlNTp7InVzZXJfaWQiOjIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5BbGxvd0FsbFVzZXJzTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiODI2ZDYzMjk1Mjg3M2VhZTlkY2Q5OGJlYTZmNDk5ZGJlM2JjODFlZSJ9', '2019-09-06 03:33:10.123090');

-- --------------------------------------------------------

--
-- Table structure for table `ManageUsers_user_profile`
--

CREATE TABLE IF NOT EXISTS `ManageUsers_user_profile` (
  `id` int(11) NOT NULL,
  `run_limit` int(11) NOT NULL,
  `file_limit` int(11) NOT NULL,
  `file_size_gb` int(11) NOT NULL,
  `approved_admin_id` int(11) NOT NULL,
  `img_profile` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ManageUsers_user_profile`
--

INSERT INTO `ManageUsers_user_profile` (`id`, `run_limit`, `file_limit`, `file_size_gb`, `approved_admin_id`, `img_profile`, `user_id`) VALUES
(1, 0, 0, 0, 0, 'img_profile/blank_profile.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `Project_dataset`
--

CREATE TABLE IF NOT EXISTS `Project_dataset` (
  `id` int(11) NOT NULL,
  `dataset_name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Project_dataset`
--

INSERT INTO `Project_dataset` (`id`, `dataset_name`, `user_id`) VALUES
(4, '15082019112304', 2),
(5, '15082019130939', 2),
(6, '15082019135330', 2),
(7, '15082019141405', 2),
(8, '15082019142740', 2),
(9, '15082019143000', 2),
(10, '16082019160913', 2),
(11, '19082019103627', 2),
(12, '19082019121235', 2),
(13, '19082019140408', 2),
(14, '19082019141917', 2),
(15, '19082019160201', 2),
(16, '23082019103131', 2),
(17, '28082019132339', 2),
(18, '30082019105932', 2),
(19, '02092019160808', 2),
(20, '11092019141539', 2),
(21, '26092019160742', 2),
(22, '04102019131515', 2),
(23, '04102019132809', 2),
(24, '04102019133858', 2),
(25, '04102019135245', 2),
(26, '04102019142526', 2),
(27, '04102019143756', 2),
(28, '07102019174803', 2),
(29, '09102019140316', 2),
(30, '10102019093332', 2),
(31, '10102019094728', 2);

-- --------------------------------------------------------

--
-- Table structure for table `Project_dataset_fileupload`
--

CREATE TABLE IF NOT EXISTS `Project_dataset_fileupload` (
  `id` int(11) NOT NULL,
  `dataset_id` int(11) NOT NULL,
  `fileupload_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Project_dataset_fileupload`
--

INSERT INTO `Project_dataset_fileupload` (`id`, `dataset_id`, `fileupload_id`, `project_id`, `user_id`) VALUES
(1, 5, 8, 30, 2),
(2, 5, 9, 30, 2),
(15, 12, 6, 30, 2),
(16, 12, 8, 30, 2),
(17, 12, 9, 30, 2),
(18, 15, 11, 32, 2),
(19, 15, 12, 32, 2),
(20, 15, 13, 32, 2),
(27, 18, 129, 67, 2),
(28, 18, 130, 67, 2),
(29, 18, 132, 67, 2),
(30, 19, 133, 68, 2),
(31, 19, 134, 68, 2),
(32, 19, 136, 68, 2),
(61, 29, 201, 81, 2),
(62, 29, 202, 81, 2),
(63, 29, 203, 81, 2),
(64, 29, 204, 81, 2),
(65, 29, 205, 81, 2),
(66, 29, 206, 81, 2),
(67, 29, 207, 81, 2),
(68, 29, 208, 81, 2),
(69, 29, 209, 81, 2),
(70, 29, 211, 81, 2),
(71, 29, 212, 81, 2),
(76, 31, 239, 88, 2),
(77, 31, 241, 88, 2),
(78, 31, 242, 88, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Project_fileupload`
--

CREATE TABLE IF NOT EXISTS `Project_fileupload` (
  `id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `path` longtext NOT NULL,
  `size` varchar(255) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `datetime` datetime(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Project_fileupload`
--

INSERT INTO `Project_fileupload` (`id`, `file_name`, `path`, `size`, `project_id`, `user_id`, `datetime`) VALUES
(6, 's1t2t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/30/input/s1t2t16tsr2.fastq', '0.0', 30, 2, '2019-09-26 03:37:45.261204'),
(7, 's1t2t16tsr2.fastq.gz', '/home/new/DJANGO/metagenomic/src/media/storage/2/30/input/s1t2t16tsr2.fastq.gz', '0.0', 30, 2, '2019-09-26 03:37:45.261204'),
(8, 's1t3t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/30/input/s1t3t16tsr2.fastq', '0.0', 30, 2, '2019-09-26 03:37:45.261204'),
(9, 's1t4t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/30/input/s1t4t16tsr2.fastq', '0.0', 30, 2, '2019-09-26 03:37:45.261204'),
(10, 'fileoligo.oligo', '/home/new/DJANGO/metagenomic/src/media/storage/2/30/input/fileoligo.oligo', '0.0', 30, 2, '2019-09-26 03:37:45.261204'),
(11, 's1t2t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/32/input/s1t2t16tsr2.fastq', '0.0', 32, 2, '2019-09-26 03:37:45.261204'),
(12, 's1t3t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/32/input/s1t3t16tsr2.fastq', '0.0', 32, 2, '2019-09-26 03:37:45.261204'),
(13, 's1t4t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/32/input/s1t4t16tsr2.fastq', '0.0', 32, 2, '2019-09-26 03:37:45.261204'),
(14, 'fileoligo.oligo', '/home/new/DJANGO/metagenomic/src/media/storage/2/32/input/fileoligo.oligo', '0.0', 32, 2, '2019-09-26 03:37:45.261204'),
(111, 'fileoligo.oligo', '/home/new/DJANGO/metagenomic/src/media/storage/2/30/input/fileoligo.oligo', '0.0', 30, 2, '2019-09-26 03:37:45.261204'),
(116, 'fileoligo.oligo', '/home/new/DJANGO/metagenomic/src/media/storage/2/31/input/fileoligo.oligo', '80.0', 31, 2, '2019-09-26 03:37:45.261204'),
(127, 'file.desing', '/home/new/DJANGO/metagenomic/src/media/storage/2/30/input/file.desing', '87.0', 30, 2, '2019-09-26 03:37:45.261204'),
(128, 'file.metadata', '/home/new/DJANGO/metagenomic/src/media/storage/2/30/input/file.metadata', '33.0', 30, 2, '2019-09-26 10:37:45.261204'),
(129, 's1t4t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/67/input/s1t4t16tsr2.fastq', '0.0', 67, 2, '2019-09-26 03:37:45.261204'),
(130, 's1t3t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/67/input/s1t3t16tsr2.fastq', '0.0', 67, 2, '2019-09-26 03:37:45.261204'),
(131, 's1t2t16tsr2.fastq.gz', '/home/new/DJANGO/metagenomic/src/media/storage/2/67/input/s1t2t16tsr2.fastq.gz', '0.0', 67, 2, '2019-09-26 03:37:45.261204'),
(132, 's1t2t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/67/input/s1t2t16tsr2.fastq', '0.0', 67, 2, '2019-09-26 03:37:45.261204'),
(133, 's1t4t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/68/input/s1t4t16tsr2.fastq', '0.0', 68, 2, '2019-09-26 03:37:45.261204'),
(134, 's1t3t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/68/input/s1t3t16tsr2.fastq', '0.0', 68, 2, '2019-09-26 03:37:45.261204'),
(135, 's1t2t16tsr2.fastq.gz', '/home/new/DJANGO/metagenomic/src/media/storage/2/68/input/s1t2t16tsr2.fastq.gz', '0.0', 68, 2, '2019-09-26 03:37:45.261204'),
(136, 's1t2t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/68/input/s1t2t16tsr2.fastq', '0.0', 68, 2, '2019-09-26 03:37:45.261204'),
(142, 'file.design', '/home/new/DJANGO/metagenomic/src/media/storage/2/68/input/file.design', '87.0', 68, 2, '2019-09-26 03:37:45.261204'),
(143, 'file.metadata', '/home/new/DJANGO/metagenomic/src/media/storage/2/68/input/file.metadata', '39.0', 68, 2, '2019-09-26 03:37:45.261204'),
(159, 'file.design', '/home/new/DJANGO/metagenomic/src/media/storage/2/67/input/file.design', '87.0', 67, 2, '2019-10-03 02:54:08.429985'),
(186, 's1t4t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/s1t4t16tsr2.fastq', '0.0', 81, 2, '2019-10-07 03:27:53.822914'),
(187, 's1t3t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/s1t3t16tsr2.fastq', '0.0', 81, 2, '2019-10-07 03:27:53.827578'),
(188, 's1t2t16tsr2.fastq.gz', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/s1t2t16tsr2.fastq.gz', '0.0', 81, 2, '2019-10-07 03:27:53.828992'),
(189, 's1t2t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/s1t2t16tsr2.fastq', '0.0', 81, 2, '2019-10-07 03:27:53.841309'),
(190, 'fileoligo.oligo', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/fileoligo.oligo', '183.0', 81, 2, '2019-10-07 03:51:35.952041'),
(191, 'fileoligo.oligos', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/fileoligo.oligos', '169.0', 81, 2, '2019-10-07 06:29:28.560574'),
(192, 's1tR1t.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/s1tR1t.fastq', '0.0', 81, 2, '2019-10-07 08:02:07.147879'),
(201, 'forward.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/forward.fastq', '0.0', 81, 2, '2019-10-08 05:01:52.551234'),
(202, 'barcodes.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/barcodes.fastq', '0.0', 81, 2, '2019-10-08 06:16:11.181204'),
(203, 'reverse.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/reverse.fastq', '0.0', 81, 2, '2019-10-08 06:16:22.813320'),
(204, 'forward.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/forward.fastq', '0.0', 81, 2, '2019-10-08 07:55:18.842561'),
(205, 'forward.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/forward.fastq', '6243953.0', 81, 2, '2019-10-08 08:00:43.820572'),
(206, 'forward.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/forward.fastq', '62439521.0', 81, 2, '2019-10-08 08:09:30.464879'),
(207, 'forward.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/forward.fastq', '62439521.0', 81, 2, '2019-10-09 04:02:22.275493'),
(208, 'reverse.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/reverse.fastq', '0.0', 81, 2, '2019-10-09 04:02:41.427422'),
(209, 'barcodes.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/barcodes.fastq', '0.0', 81, 2, '2019-10-09 04:02:44.152923'),
(210, 's1tR1ttte.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/s1tR1ttte.fastq', '0.0', 81, 2, '2019-10-09 04:14:48.024512'),
(211, 'forward.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/forward.fastq', '62439521.0', 81, 2, '2019-10-09 04:15:25.131318'),
(212, 'forward.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/forward.fastq', '62439521.0', 81, 2, '2019-10-09 04:26:04.349477'),
(213, 'fileoligo.oligos', '/home/new/DJANGO/metagenomic/src/media/storage/2/81/input/fileoligo.oligos', '165.0', 81, 2, '2019-10-09 04:26:11.961427'),
(237, 'barcodes.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/88/input/barcodes.fastq', '0.0', 88, 2, '2019-10-10 02:46:59.780982'),
(238, 'reverse.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/88/input/reverse.fastq', '0.0', 88, 2, '2019-10-10 02:46:59.782167'),
(239, 's1t2t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/88/input/s1t2t16tsr2.fastq', '0.0', 88, 2, '2019-10-10 02:46:59.791970'),
(240, 's1t2t16tsr2.fastq.gz', '/home/new/DJANGO/metagenomic/src/media/storage/2/88/input/s1t2t16tsr2.fastq.gz', '0.0', 88, 2, '2019-10-10 02:46:59.797373'),
(241, 's1t3t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/88/input/s1t3t16tsr2.fastq', '0.0', 88, 2, '2019-10-10 02:46:59.800489'),
(242, 's1t4t16tsr2.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/88/input/s1t4t16tsr2.fastq', '0.0', 88, 2, '2019-10-10 02:46:59.805024'),
(243, 's1tR1ttte.fastq', '/home/new/DJANGO/metagenomic/src/media/storage/2/88/input/s1tR1ttte.fastq', '0.0', 88, 2, '2019-10-10 02:46:59.815951'),
(244, 'fileoligo.oligos', '/home/new/DJANGO/metagenomic/src/media/storage/2/88/input/fileoligo.oligos', '0.0', 88, 2, '2019-10-10 02:47:02.000116'),
(245, 'map.tsv', '/home/new/DJANGO/metagenomic/src/media/storage/2/88/input/map.tsv', '127.0', 88, 2, '2019-10-10 04:07:09.532593');

-- --------------------------------------------------------

--
-- Table structure for table `Project_project`
--

CREATE TABLE IF NOT EXISTS `Project_project` (
  `project_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `project_title` varchar(255) NOT NULL,
  `project_detail` longtext NOT NULL,
  `project_permission` varchar(30) NOT NULL,
  `date_start` date DEFAULT NULL,
  `data_type` varchar(255) DEFAULT NULL,
  `ngs_platform` varchar(255) DEFAULT NULL,
  `program` varchar(255) DEFAULT NULL,
  `sub_program` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Project_project`
--

INSERT INTO `Project_project` (`project_id`, `project_name`, `project_title`, `project_detail`, `project_permission`, `date_start`, `data_type`, `ngs_platform`, `program`, `sub_program`, `user_id`) VALUES
(29, 'q2', 'q2', 'test', 'Private', '2019-07-18', '16s', 'proton_barcodes_fasta', 'Qiime', 'Qiime2-vsearch', 2),
(30, 'run_mothur', 'OTU', 'mothur', 'Private', '2019-07-18', '16s', 'miseq_barcodes_primers', 'Mothur', 'Phylotype', 2),
(31, 'q1', 'q1', 'q1', 'Private', '2019-07-18', '16s', 'miseq_contain_primer', 'Qiime', 'Qiime1', 2),
(32, 'Mothur+Qiime1', 'test', 'test', 'Private', '2019-07-18', '16s', 'proton_without', 'Mothur+Qiime1', NULL, 2),
(67, 'test', 'test', 'test', 'Private', '2019-08-29', '16s', 'miseq_without_barcodes', 'Mothur', 'Phylotype', 2),
(68, 'test2', 'test2', 'test2', 'Private', '2019-09-02', '16s', 'miseq_barcodes_primers', 'Mothur', 'OTU', 2),
(81, 'Q2Deblur', 'test', 'test', 'Private', '2019-10-07', '16s', 'miseq_barcodes_primers', 'Qiime', 'Qiime2-deblur', 2),
(88, 'MQ1', 'test', 'test', 'Private', '2019-10-10', '18s', 'miseq_contain_primer', 'Mothur+Qiime1', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Project_share`
--

CREATE TABLE IF NOT EXISTS `Project_share` (
  `project_share_id` int(11) NOT NULL,
  `owner_user_id` int(11) NOT NULL,
  `share_user_id` int(11) NOT NULL,
  `permission` longtext NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Project_status`
--

CREATE TABLE IF NOT EXISTS `Project_status` (
  `status_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `step` int(11) NOT NULL,
  `to_path` varchar(255) NOT NULL,
  `job_id` int(11) NOT NULL,
  `upload` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `generate_report` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Project_status`
--

INSERT INTO `Project_status` (`status_id`, `status`, `step`, `to_path`, `job_id`, `upload`, `project_id`, `generate_report`) VALUES
(27, 0, 0, '/RunPipeline/Qiime2SelectResult/', 0, 0, 29, 0),
(28, 0, 0, '/RunPipeline/SelectResult/', 0, 0, 30, 0),
(29, 0, 0, '/RunPipeline/QiimeSelectResult/', 0, 0, 31, 0),
(30, 0, 0, '/RunPipeline/MothurQiimeSelectResult/', 0, 0, 32, 0),
(37, 0, 0, '/RunPipeline/SelectResult/', 0, 0, 67, 0),
(38, 0, 0, '/RunPipeline/SelectResult/', 0, 0, 68, 0),
(51, 0, 0, '/RunPipeline/Qiime2PreProcess/', 0, 0, 81, 0),
(58, 0, 0, '/RunPipeline/MothurQiimePreprocess/', 0, 0, 88, 0);

-- --------------------------------------------------------

--
-- Table structure for table `RunPipeline_mothur_qiime1_s1_preprocess`
--

CREATE TABLE IF NOT EXISTS `RunPipeline_mothur_qiime1_s1_preprocess` (
  `id` int(11) NOT NULL,
  `maximum_ambiguous` varchar(255) DEFAULT NULL,
  `maximum_homopolymer` varchar(255) DEFAULT NULL,
  `minimum_reads_length` varchar(255) DEFAULT NULL,
  `maximum_reads_length` varchar(255) DEFAULT NULL,
  `alignment` varchar(255) DEFAULT NULL,
  `silva` varchar(255) DEFAULT NULL,
  `fn_upload` varchar(255) DEFAULT NULL,
  `pre_cluster` varchar(255) DEFAULT NULL,
  `prepare_taxo_classification` varchar(255) DEFAULT NULL,
  `cutoff` varchar(255) DEFAULT NULL,
  `taxon_elimination` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `RunPipeline_mothur_qiime1_s1_preprocess`
--

INSERT INTO `RunPipeline_mothur_qiime1_s1_preprocess` (`id`, `maximum_ambiguous`, `maximum_homopolymer`, `minimum_reads_length`, `maximum_reads_length`, `alignment`, `silva`, `fn_upload`, `pre_cluster`, `prepare_taxo_classification`, `cutoff`, `taxon_elimination`, `date`, `project_id`, `user_id`) VALUES
(4, '8', '8', '100', '260', 'Silva', 'Default??', '', '2', 'Silva', '80', 'default', '2019-08-29', 32, 2);

-- --------------------------------------------------------

--
-- Table structure for table `RunPipeline_mothur_qiime1_s2_pick`
--

CREATE TABLE IF NOT EXISTS `RunPipeline_mothur_qiime1_s2_pick` (
  `id` int(11) NOT NULL,
  `group_dirversity` varchar(255) DEFAULT NULL,
  `non_phylogenetic_metrics` varchar(255) DEFAULT NULL,
  `phylogenetic_metrics` varchar(255) DEFAULT NULL,
  `permanova` varchar(255) DEFAULT NULL,
  `sub_permanova` varchar(255) DEFAULT NULL,
  `anosim` varchar(255) DEFAULT NULL,
  `sub_anosim` varchar(255) DEFAULT NULL,
  `adonis` varchar(255) DEFAULT NULL,
  `sub_adonis` varchar(255) DEFAULT NULL,
  `pathway` varchar(255) DEFAULT NULL,
  `sample_comparison` varchar(255) DEFAULT NULL,
  `statistical_test` varchar(255) DEFAULT NULL,
  `ci_method` varchar(255) DEFAULT NULL,
  `p_value` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `RunPipeline_mothur_qiime1_s2_pick`
--

INSERT INTO `RunPipeline_mothur_qiime1_s2_pick` (`id`, `group_dirversity`, `non_phylogenetic_metrics`, `phylogenetic_metrics`, `permanova`, `sub_permanova`, `anosim`, `sub_anosim`, `adonis`, `sub_adonis`, `pathway`, `sample_comparison`, `statistical_test`, `ci_method`, `p_value`, `date`, `project_id`, `user_id`) VALUES
(4, 'None', 'abund_jaccard', 'weighted_unifrac', 'None', 'None', 'None', 'None', 'None', 'None', NULL, NULL, NULL, NULL, NULL, '2019-08-29', 32, 2);

-- --------------------------------------------------------

--
-- Table structure for table `RunPipeline_mothur_s1_preprocess`
--

CREATE TABLE IF NOT EXISTS `RunPipeline_mothur_s1_preprocess` (
  `id` int(11) NOT NULL,
  `maximum_ambiguous` varchar(255) DEFAULT NULL,
  `maximum_homopolymer` varchar(255) DEFAULT NULL,
  `minimum_reads_length` varchar(255) DEFAULT NULL,
  `maximum_reads_length` varchar(255) DEFAULT NULL,
  `alignment` varchar(255) DEFAULT NULL,
  `silva` varchar(255) DEFAULT NULL,
  `fn_upload` varchar(255) DEFAULT NULL,
  `pre_cluster` varchar(255) DEFAULT NULL,
  `prepare_taxo_classification` varchar(255) DEFAULT NULL,
  `cutoff` varchar(255) DEFAULT NULL,
  `taxon_elimination` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `RunPipeline_mothur_s1_preprocess`
--

INSERT INTO `RunPipeline_mothur_s1_preprocess` (`id`, `maximum_ambiguous`, `maximum_homopolymer`, `minimum_reads_length`, `maximum_reads_length`, `alignment`, `silva`, `fn_upload`, `pre_cluster`, `prepare_taxo_classification`, `cutoff`, `taxon_elimination`, `date`, `project_id`, `user_id`) VALUES
(23, '8', '8', '100', '260', 'Silva', 'silva.v123.fasta', '', '2', 'Silva', '80', 'default', '2019-09-12', 30, 2),
(27, '8', '8', '100', '260', 'Silva', 'Default??', '', '2', 'Silva', '80', 'default', '2019-09-23', 68, 2),
(36, '8', '8', '100', '260', 'Silva', 'Default??', '', '2', 'Greengene', '80', 'default', '2019-10-04', 67, 2);

-- --------------------------------------------------------

--
-- Table structure for table `RunPipeline_mothur_s2_prepare`
--

CREATE TABLE IF NOT EXISTS `RunPipeline_mothur_s2_prepare` (
  `id` int(11) NOT NULL,
  `sub_sample` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `RunPipeline_mothur_s2_prepare`
--

INSERT INTO `RunPipeline_mothur_s2_prepare` (`id`, `sub_sample`, `date`, `project_id`, `user_id`) VALUES
(21, '1500', '2019-09-12', 30, 2),
(25, '', '2019-09-23', 68, 2),
(33, '10391', '2019-10-04', 67, 2);

-- --------------------------------------------------------

--
-- Table structure for table `RunPipeline_mothur_s3_analysis`
--

CREATE TABLE IF NOT EXISTS `RunPipeline_mothur_s3_analysis` (
  `id` int(11) NOT NULL,
  `database` varchar(255) DEFAULT NULL,
  `taxonomy_rank` varchar(255) DEFAULT NULL,
  `alpha_diversity` varchar(255) DEFAULT NULL,
  `beta_diversity` varchar(255) DEFAULT NULL,
  `venn_diagram` varchar(255) DEFAULT NULL,
  `structure_tree` varchar(255) DEFAULT NULL,
  `membership_tree` varchar(255) DEFAULT NULL,
  `method_use` varchar(255) DEFAULT NULL,
  `view_type` varchar(255) DEFAULT NULL,
  `structure` varchar(255) DEFAULT NULL,
  `membership` varchar(255) DEFAULT NULL,
  `design` varchar(255) DEFAULT NULL,
  `file_design` varchar(255) DEFAULT NULL,
  `name_raeOTU` varchar(255) DEFAULT NULL,
  `name_metadata` varchar(255) DEFAULT NULL,
  `name_list_raeOTU` varchar(255) DEFAULT NULL,
  `name_list_metadata` varchar(255) DEFAULT NULL,
  `file_metadata` varchar(255) DEFAULT NULL,
  `pathway` varchar(255) DEFAULT NULL,
  `sample_comparison` varchar(255) DEFAULT NULL,
  `statistical` varchar(255) DEFAULT NULL,
  `ci_method` varchar(255) DEFAULT NULL,
  `p_value` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `RunPipeline_mothur_s3_analysis`
--

INSERT INTO `RunPipeline_mothur_s3_analysis` (`id`, `database`, `taxonomy_rank`, `alpha_diversity`, `beta_diversity`, `venn_diagram`, `structure_tree`, `membership_tree`, `method_use`, `view_type`, `structure`, `membership`, `design`, `file_design`, `name_raeOTU`, `name_metadata`, `name_list_raeOTU`, `name_list_metadata`, `file_metadata`, `pathway`, `sample_comparison`, `statistical`, `ci_method`, `p_value`, `date`, `project_id`, `user_id`) VALUES
(22, 'Greengenes', '1', 'default', 'default', '', 'thetayc', 'jclass', 'PCoA', '2D', 'thetayc,thetan,morisitahorn', 'jclass', 'amova', '', 'raeOTU', 'Metadata', 'spearman', 'spearman', '', 'level_2', NULL, 'Chi-square2', 'DP2', NULL, '2019-09-12', 30, 2),
(27, 'Greengenes', '0.03', 'default', 'default', '', 'thetayc', 'jclass', 'PCoA', '2D', 'thetayc,thetan,morisitahorn', 'jclass', '', '', 'raeOTU', 'Metadata', 'spearman', 'spearman', '', 'level_2', 'A vs B', 'Fisher', 'DP1', '0.05', '2019-09-23', 68, 2),
(35, 'Greengene', '1', 'default', 'default', '', 'thetayc', 'jclass', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, '2019-10-04', 67, 2);

-- --------------------------------------------------------

--
-- Table structure for table `RunPipeline_qiiem2_s1_preprocess`
--

CREATE TABLE IF NOT EXISTS `RunPipeline_qiiem2_s1_preprocess` (
  `id` int(11) NOT NULL,
  `align_clean` varchar(255) DEFAULT NULL,
  `silva` varchar(255) DEFAULT NULL,
  `precent_identity` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `RunPipeline_qiiem2_s1_preprocess`
--

INSERT INTO `RunPipeline_qiiem2_s1_preprocess` (`id`, `align_clean`, `silva`, `precent_identity`, `date`, `project_id`, `user_id`) VALUES
(4, 'OpenReference', 'Default', '97', '2019-08-29', 29, 2);

-- --------------------------------------------------------

--
-- Table structure for table `RunPipeline_qiiem2_s2_prepare_data`
--

CREATE TABLE IF NOT EXISTS `RunPipeline_qiiem2_s2_prepare_data` (
  `id` int(11) NOT NULL,
  `sub_sample` varchar(255) DEFAULT NULL,
  `group_core_diversity` varchar(255) DEFAULT NULL,
  `permanova` varchar(255) DEFAULT NULL,
  `sub_permanova` varchar(255) DEFAULT NULL,
  `pathway` varchar(255) DEFAULT NULL,
  `sample_comparison` varchar(255) DEFAULT NULL,
  `statistical_test` varchar(255) DEFAULT NULL,
  `ci_method` varchar(255) DEFAULT NULL,
  `p_value` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `RunPipeline_qiiem2_s2_prepare_data`
--

INSERT INTO `RunPipeline_qiiem2_s2_prepare_data` (`id`, `sub_sample`, `group_core_diversity`, `permanova`, `sub_permanova`, `pathway`, `sample_comparison`, `statistical_test`, `ci_method`, `p_value`, `date`, `project_id`, `user_id`) VALUES
(5, '2202', 'None', 'None', 'None', NULL, NULL, NULL, NULL, NULL, '2019-08-29', 29, 2);

-- --------------------------------------------------------

--
-- Table structure for table `RunPipeline_qiime1_s1_process`
--

CREATE TABLE IF NOT EXISTS `RunPipeline_qiime1_s1_process` (
  `id` int(11) NOT NULL,
  `group_dirversity` varchar(255) DEFAULT NULL,
  `non_phylogenetic_metrics` varchar(255) DEFAULT NULL,
  `phylogenetic_metrics` varchar(255) DEFAULT NULL,
  `permanova` varchar(255) DEFAULT NULL,
  `sub_permanova` varchar(255) DEFAULT NULL,
  `anosim` varchar(255) DEFAULT NULL,
  `sub_anosim` varchar(255) DEFAULT NULL,
  `adonis` varchar(255) DEFAULT NULL,
  `sub_adonis` varchar(255) DEFAULT NULL,
  `pathway` varchar(255) DEFAULT NULL,
  `sample_comparison` varchar(255) DEFAULT NULL,
  `statistical_test` varchar(255) DEFAULT NULL,
  `ci_method` varchar(255) DEFAULT NULL,
  `p_value` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `RunPipeline_qiime1_s1_process`
--

INSERT INTO `RunPipeline_qiime1_s1_process` (`id`, `group_dirversity`, `non_phylogenetic_metrics`, `phylogenetic_metrics`, `permanova`, `sub_permanova`, `anosim`, `sub_anosim`, `adonis`, `sub_adonis`, `pathway`, `sample_comparison`, `statistical_test`, `ci_method`, `p_value`, `date`, `project_id`, `user_id`) VALUES
(4, 'None', 'abund_jaccard', 'weighted_unifrac', 'None', 'None', 'None', 'None', 'None', 'None', NULL, NULL, NULL, NULL, NULL, '2019-08-29', 31, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `ManageUsers_user_profile`
--
ALTER TABLE `ManageUsers_user_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `Project_dataset`
--
ALTER TABLE `Project_dataset`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Project_dataset_user_id_bd08152c_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `Project_dataset_fileupload`
--
ALTER TABLE `Project_dataset_fileupload`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Project_dataset_file_dataset_id_0871fa00_fk_Project_d` (`dataset_id`),
  ADD KEY `Project_dataset_file_fileupload_id_ec52a518_fk_Project_f` (`fileupload_id`),
  ADD KEY `Project_dataset_file_project_id_89bfc04d_fk_Project_p` (`project_id`),
  ADD KEY `Project_dataset_fileupload_user_id_cda7b2d8_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `Project_fileupload`
--
ALTER TABLE `Project_fileupload`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Project_fileupload_project_id_682c2b1c_fk_Project_p` (`project_id`),
  ADD KEY `Project_fileupload_user_id_c12155b8_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `Project_project`
--
ALTER TABLE `Project_project`
  ADD PRIMARY KEY (`project_id`),
  ADD KEY `Project_project_user_id_b48132cb_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `Project_share`
--
ALTER TABLE `Project_share`
  ADD PRIMARY KEY (`project_share_id`),
  ADD KEY `Project_share_project_id_5db455cf_fk_Project_project_project_id` (`project_id`);

--
-- Indexes for table `Project_status`
--
ALTER TABLE `Project_status`
  ADD PRIMARY KEY (`status_id`),
  ADD KEY `Project_status_project_id_540fcc8a_fk_Project_project_project_id` (`project_id`);

--
-- Indexes for table `RunPipeline_mothur_qiime1_s1_preprocess`
--
ALTER TABLE `RunPipeline_mothur_qiime1_s1_preprocess`
  ADD PRIMARY KEY (`id`),
  ADD KEY `RunPipeline_mothur_q_project_id_b6f7e7af_fk_Project_p` (`project_id`),
  ADD KEY `RunPipeline_mothur_q_user_id_84addeae_fk_auth_user` (`user_id`);

--
-- Indexes for table `RunPipeline_mothur_qiime1_s2_pick`
--
ALTER TABLE `RunPipeline_mothur_qiime1_s2_pick`
  ADD PRIMARY KEY (`id`),
  ADD KEY `RunPipeline_mothur_q_project_id_2d1a52b7_fk_Project_p` (`project_id`),
  ADD KEY `RunPipeline_mothur_q_user_id_546fbef3_fk_auth_user` (`user_id`);

--
-- Indexes for table `RunPipeline_mothur_s1_preprocess`
--
ALTER TABLE `RunPipeline_mothur_s1_preprocess`
  ADD PRIMARY KEY (`id`),
  ADD KEY `RunPipeline_mothur_s_project_id_866e20a1_fk_Project_p` (`project_id`),
  ADD KEY `RunPipeline_mothur_s_user_id_d87937d7_fk_auth_user` (`user_id`);

--
-- Indexes for table `RunPipeline_mothur_s2_prepare`
--
ALTER TABLE `RunPipeline_mothur_s2_prepare`
  ADD PRIMARY KEY (`id`),
  ADD KEY `RunPipeline_mothur_s_project_id_67064085_fk_Project_p` (`project_id`),
  ADD KEY `RunPipeline_mothur_s2_prepare_user_id_6e5d24d1_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `RunPipeline_mothur_s3_analysis`
--
ALTER TABLE `RunPipeline_mothur_s3_analysis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `RunPipeline_mothur_s_project_id_982f22d1_fk_Project_p` (`project_id`),
  ADD KEY `RunPipeline_mothur_s3_prepare_user_id_60386259_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `RunPipeline_qiiem2_s1_preprocess`
--
ALTER TABLE `RunPipeline_qiiem2_s1_preprocess`
  ADD PRIMARY KEY (`id`),
  ADD KEY `RunPipeline_qiiem2_s_project_id_d5ad4518_fk_Project_p` (`project_id`),
  ADD KEY `RunPipeline_qiiem2_s_user_id_10aee7be_fk_auth_user` (`user_id`);

--
-- Indexes for table `RunPipeline_qiiem2_s2_prepare_data`
--
ALTER TABLE `RunPipeline_qiiem2_s2_prepare_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `RunPipeline_qiiem2_s_project_id_b65fcdc8_fk_Project_p` (`project_id`),
  ADD KEY `RunPipeline_qiiem2_s_user_id_133815c9_fk_auth_user` (`user_id`);

--
-- Indexes for table `RunPipeline_qiime1_s1_process`
--
ALTER TABLE `RunPipeline_qiime1_s1_process`
  ADD PRIMARY KEY (`id`),
  ADD KEY `RunPipeline_qiime1_s_project_id_e7315c59_fk_Project_p` (`project_id`),
  ADD KEY `RunPipeline_qiime1_s1_process_user_id_67bb1520_fk_auth_user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `ManageUsers_user_profile`
--
ALTER TABLE `ManageUsers_user_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Project_dataset`
--
ALTER TABLE `Project_dataset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `Project_dataset_fileupload`
--
ALTER TABLE `Project_dataset_fileupload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `Project_fileupload`
--
ALTER TABLE `Project_fileupload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=246;
--
-- AUTO_INCREMENT for table `Project_project`
--
ALTER TABLE `Project_project`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `Project_share`
--
ALTER TABLE `Project_share`
  MODIFY `project_share_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Project_status`
--
ALTER TABLE `Project_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `RunPipeline_mothur_qiime1_s1_preprocess`
--
ALTER TABLE `RunPipeline_mothur_qiime1_s1_preprocess`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `RunPipeline_mothur_qiime1_s2_pick`
--
ALTER TABLE `RunPipeline_mothur_qiime1_s2_pick`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `RunPipeline_mothur_s1_preprocess`
--
ALTER TABLE `RunPipeline_mothur_s1_preprocess`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `RunPipeline_mothur_s2_prepare`
--
ALTER TABLE `RunPipeline_mothur_s2_prepare`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `RunPipeline_mothur_s3_analysis`
--
ALTER TABLE `RunPipeline_mothur_s3_analysis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `RunPipeline_qiiem2_s1_preprocess`
--
ALTER TABLE `RunPipeline_qiiem2_s1_preprocess`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `RunPipeline_qiiem2_s2_prepare_data`
--
ALTER TABLE `RunPipeline_qiiem2_s2_prepare_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `RunPipeline_qiime1_s1_process`
--
ALTER TABLE `RunPipeline_qiime1_s1_process`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `ManageUsers_user_profile`
--
ALTER TABLE `ManageUsers_user_profile`
  ADD CONSTRAINT `ManageUsers_user_profile_user_id_95f3aaef_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `Project_dataset`
--
ALTER TABLE `Project_dataset`
  ADD CONSTRAINT `Project_dataset_user_id_bd08152c_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `Project_dataset_fileupload`
--
ALTER TABLE `Project_dataset_fileupload`
  ADD CONSTRAINT `Project_dataset_file_dataset_id_0871fa00_fk_Project_d` FOREIGN KEY (`dataset_id`) REFERENCES `Project_dataset` (`id`),
  ADD CONSTRAINT `Project_dataset_file_fileupload_id_ec52a518_fk_Project_f` FOREIGN KEY (`fileupload_id`) REFERENCES `Project_fileupload` (`id`),
  ADD CONSTRAINT `Project_dataset_file_project_id_89bfc04d_fk_Project_p` FOREIGN KEY (`project_id`) REFERENCES `Project_project` (`project_id`),
  ADD CONSTRAINT `Project_dataset_fileupload_user_id_cda7b2d8_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `Project_fileupload`
--
ALTER TABLE `Project_fileupload`
  ADD CONSTRAINT `Project_fileupload_project_id_682c2b1c_fk_Project_p` FOREIGN KEY (`project_id`) REFERENCES `Project_project` (`project_id`),
  ADD CONSTRAINT `Project_fileupload_user_id_c12155b8_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `Project_project`
--
ALTER TABLE `Project_project`
  ADD CONSTRAINT `Project_project_user_id_b48132cb_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `Project_share`
--
ALTER TABLE `Project_share`
  ADD CONSTRAINT `Project_share_project_id_5db455cf_fk_Project_project_project_id` FOREIGN KEY (`project_id`) REFERENCES `Project_project` (`project_id`);

--
-- Constraints for table `Project_status`
--
ALTER TABLE `Project_status`
  ADD CONSTRAINT `Project_status_project_id_540fcc8a_fk_Project_project_project_id` FOREIGN KEY (`project_id`) REFERENCES `Project_project` (`project_id`);

--
-- Constraints for table `RunPipeline_mothur_qiime1_s1_preprocess`
--
ALTER TABLE `RunPipeline_mothur_qiime1_s1_preprocess`
  ADD CONSTRAINT `RunPipeline_mothur_q_project_id_b6f7e7af_fk_Project_p` FOREIGN KEY (`project_id`) REFERENCES `Project_project` (`project_id`),
  ADD CONSTRAINT `RunPipeline_mothur_q_user_id_84addeae_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `RunPipeline_mothur_qiime1_s2_pick`
--
ALTER TABLE `RunPipeline_mothur_qiime1_s2_pick`
  ADD CONSTRAINT `RunPipeline_mothur_q_project_id_2d1a52b7_fk_Project_p` FOREIGN KEY (`project_id`) REFERENCES `Project_project` (`project_id`),
  ADD CONSTRAINT `RunPipeline_mothur_q_user_id_546fbef3_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `RunPipeline_mothur_s1_preprocess`
--
ALTER TABLE `RunPipeline_mothur_s1_preprocess`
  ADD CONSTRAINT `RunPipeline_mothur_s_project_id_866e20a1_fk_Project_p` FOREIGN KEY (`project_id`) REFERENCES `Project_project` (`project_id`),
  ADD CONSTRAINT `RunPipeline_mothur_s_user_id_d87937d7_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `RunPipeline_mothur_s2_prepare`
--
ALTER TABLE `RunPipeline_mothur_s2_prepare`
  ADD CONSTRAINT `RunPipeline_mothur_s2_prepare_user_id_6e5d24d1_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `RunPipeline_mothur_s_project_id_67064085_fk_Project_p` FOREIGN KEY (`project_id`) REFERENCES `Project_project` (`project_id`);

--
-- Constraints for table `RunPipeline_mothur_s3_analysis`
--
ALTER TABLE `RunPipeline_mothur_s3_analysis`
  ADD CONSTRAINT `RunPipeline_mothur_s3_prepare_user_id_60386259_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `RunPipeline_mothur_s_project_id_982f22d1_fk_Project_p` FOREIGN KEY (`project_id`) REFERENCES `Project_project` (`project_id`);

--
-- Constraints for table `RunPipeline_qiiem2_s1_preprocess`
--
ALTER TABLE `RunPipeline_qiiem2_s1_preprocess`
  ADD CONSTRAINT `RunPipeline_qiiem2_s_project_id_d5ad4518_fk_Project_p` FOREIGN KEY (`project_id`) REFERENCES `Project_project` (`project_id`),
  ADD CONSTRAINT `RunPipeline_qiiem2_s_user_id_10aee7be_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `RunPipeline_qiiem2_s2_prepare_data`
--
ALTER TABLE `RunPipeline_qiiem2_s2_prepare_data`
  ADD CONSTRAINT `RunPipeline_qiiem2_s_project_id_b65fcdc8_fk_Project_p` FOREIGN KEY (`project_id`) REFERENCES `Project_project` (`project_id`),
  ADD CONSTRAINT `RunPipeline_qiiem2_s_user_id_133815c9_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `RunPipeline_qiime1_s1_process`
--
ALTER TABLE `RunPipeline_qiime1_s1_process`
  ADD CONSTRAINT `RunPipeline_qiime1_s1_process_user_id_67bb1520_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `RunPipeline_qiime1_s_project_id_e7315c59_fk_Project_p` FOREIGN KEY (`project_id`) REFERENCES `Project_project` (`project_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
