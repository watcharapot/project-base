from django.conf.urls import url
from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth.decorators import login_required
from django.conf.urls.static import static
from django.conf import settings
from . import views
from django.views.static import serve
from django.conf import settings
from MainApp.helper import decode_url
from django.shortcuts import redirect

app_name = 'MainApp'

# @login_required
# def protected_serve(request, path, document_root=None, show_indexes=False):
#     if 'storage' in path:
#         pathh = path.split('/')
#         user_id = str(request.user.id)
#         check_uid = decode_url(pathh[2]).split('.')
#         if check_uid[0] != user_id:
#             return redirect('/ManageUsers/logout/')
#     return serve(request, path, document_root, show_indexes)

# set url with function in view Metagenomic

urlpatterns = [
    url(r'^$', views.index),
    url(r'^home', views.index, name='home'),
    url(r'^dashboard', views.dashboard, name='dashboard'),
    url(r'^about', views.about, name='about'),
    url(r'^contact', views.contact, name='contact'),
    url(r'^help', views.help, name='help'),
    # url(r'^run_celery_email_ncbi', views.run_celery_email_ncbi),
    path('check_celery/<str:celery_id>/', views.check_celery_id),



    # url(r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], protected_serve, {'document_root': settings.MEDIA_ROOT}),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
