from django.apps import AppConfig


class DigitalbiobankappConfig(AppConfig):
    name = 'MetaGenomics'
