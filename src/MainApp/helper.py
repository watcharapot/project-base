import base64, random, string, os # encode
from django.conf import settings
from django.shortcuts import render, redirect,HttpResponseRedirect
from django.urls import resolve
import datetime
# minio
from minio import Minio
from minio.error import ResponseError
import urllib3
import boto3
# avro
import time
import json
# from confluent_kafka import avro
# from confluent_kafka.avro import AvroProducer

minio_bucket_name = settings.MINIO_BUCKET_NAME

def encode_url(number):
    encode = base64.b16encode(base64.b85encode(bytes(str(number), "utf-8"))).decode("utf-8", "ignore")
    return encode

def decode_url(number):
    decode = base64.b85decode(base64.b16decode(number)).decode("utf-8", "ignore")
    return decode

def to_path(path,pid):
    from Project.models import Status
    Status.objects.filter(project_id=pid).update(to_path=str(path))
    # to_path('/RunPipeline/MothurPreprocess/',decode(project_id))

def check_path(request,pid):
    from Project.models import Status
    go_to = Status.objects.filter(project_id=pid).values('to_path')
    go = str(go_to[0]['to_path']+encode_url(pid)+'/')
    current_path = go_to[0]['to_path']
    current_url = '/'+resolve(request.path_info).namespace+'/'+resolve(request.path_info).url_name+'/'
    if current_path != current_url:
        return HttpResponseRedirect(go)
    return False
        

    # check =  check_path_sra(request,decode(project_id))
    # if check:
    #     return check

def this_now():
	now = datetime.datetime.now()
	return now.strftime("%Y-%m-%d")

def parseNode(nwString):
    parenCount = 0
    tree = ''
    processed = ''
    index = 0
    for char in nwString:
        if char == "(":
            parenCount += 1
            if parenCount == 1:
                continue
        elif char == ")":
            parenCount -= 1
            if parenCount == 0:
                if index + 2 > len(nwString):
                    break
                else:
                    tree = nwString[index + 2:]
                    break
 
        if char == ",":
            if parenCount != 1:
                processed += "|"
            else:
                processed += ","
        else:
            processed += char
 
        index += 1

    data = processed.split(',')
 
    for i in range(len(data)):
        data[i] = data[i].replace('|',',')
 
    t = tree.strip()
    if t.find(":") == -1:
        label = t
        dist = ""
    else:
        label = t[:t.find(":")]
        dist = t[t.find(":")+1:]
 
    return (label, dist, data)
 
def recurseBuild(nwString):
    nwString = nwString.replace(";","")
    if nwString.find('(') == -1:
        if len(nwString.split(',')) == 1:
            if nwString.find(":") == -1:
                label = nwString
                dist = ""
            else:
                label = nwString[:nwString.find(":")]
                dist = float(nwString[nwString.find(":")+1:])
            return {"name":label,"value":dist}
        else:
            return nwString.split(',')
    else:
        label, dist, data = parseNode(nwString)
 
        dataArray = []
        for item in data:
            dataArray.append(recurseBuild(item))
 
        return {"name":label,"value":dist,"children":dataArray}

def connect_minio():
    # connect to minio #######################
    try:
        minioClient = Minio(settings.MINIO_URL,
                      access_key=settings.MINIO_ACCESS_KEY,
                      secret_key=settings.MINIO_SECRET_KEY,
                      secure=settings.MINIO_SECURE,
                    )
    except ResponseError as err:    
        # print(err)
        pass

    return minioClient

def prefix_check_minio(minio_bucket_name,user_id,project_id,file_name):
    minioClient = connect_minio()
    objects = minioClient.list_objects_v2(minio_bucket_name, prefix=user_id+'/'+project_id+'/'+file_name+'/')
    check = {'output': False}
    for obj in objects:
        if 'output' in obj.object_name:
            check = {'output': True}
            break
        else:
            check = {'output': False}

    return check

def minio_list_objects(path):
    minioClient = connect_minio()
    objects = minioClient.list_objects(minio_bucket_name, prefix=path,recursive=False)
    data = []
    for obj in objects:
        data.append(obj.object_name)

    return data

def boto3_connect_minio():
    session = boto3.session.Session()
    
    boto_s3_client = session.client(
        service_name='s3',
        aws_access_key_id=settings.MINIO_ACCESS_KEY,
        aws_secret_access_key=settings.MINIO_SECRET_KEY,
        endpoint_url='http://'+settings.MINIO_URL,
    )
    return boto_s3_client

def readfile_minio_fix_output(full_path_name):
    try:
        minioClient = connect_minio()
        datas = minioClient.get_object(minio_bucket_name, full_path_name)
        data = None
        result = str(datas.data.decode('utf-8'))
        data = result.split('\n')
    except :
        data = None

    return data

def readFile_minio(file_name,project_id,user_id,IOput,diry):
    try:
        minioClient = connect_minio()
        if IOput == 'input':
            datas = minioClient.get_object(minio_bucket_name, user_id+'/'+project_id+'/'+IOput+'/'+diry+file_name)
        elif IOput == 'output':
            datas = minioClient.get_object(minio_bucket_name, user_id+'/'+project_id+'/'+IOput+'/'+diry+file_name)
        elif IOput == 'custom':
            datas = minioClient.get_object(minio_bucket_name, user_id+'/'+project_id+diry+file_name)
        data = None
        result = str(datas.data.decode('utf-8'))
        data = result.split('\n')
    except :
        data = None

    return data

def split_filename(file_full_path):
    filename = (file_full_path.split('/'))[-1]
    return filename

def produce_avro(topic_name,meg_key,meg_value):
    topic = str(topic_name)

    def delivery_report(err, msg):
        """ Called once for each message produced to indicate delivery result.
            Triggered by poll() or flush(). """
        if err is not None:
            print('Message delivery failed: {}'.format(err))
        else:
            print('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))

    conf = {
        "bootstrap.servers": "10.227.52.245:31090,10.227.52.246:31091,10.227.52.247:31092",
        "on_delivery": delivery_report,
        "schema.registry.url": "http://10.227.52.247:30553"
    }


    value_schema = avro.load(settings.MEDIA_ROOT+"/storage/schemas/{}-value.avsc".format(topic))
    key_schema = avro.load(settings.MEDIA_ROOT+"/storage/schemas/{}-key.avsc".format(topic))
    
    avroProducer = AvroProducer(conf, default_key_schema=key_schema, default_value_schema=value_schema)

    # key = json.dumps(meg_key)
    # value = json.dumps(meg_value)

    # print(meg_key)
    # print(meg_value)
    avroProducer.produce(topic=topic, key=meg_key, value=meg_value)
    avroProducer.poll(0)
    avroProducer.flush()

    return 'ok'











    