from django.db import models
from django.contrib.auth.models import User , Group
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.timezone import now

class user_profile(models.Model):
    user = models.OneToOneField(User ,on_delete=models.PROTECT)
    nationd_id = models.CharField(max_length=255, blank=True, null=True)
    mobile_tel = models.CharField(max_length=255, blank=True, null=True)
    organization = models.CharField(max_length=255, blank=True, null=True)
    sub_organization = models.CharField(max_length=255, blank=True, null=True)
    department = models.CharField(max_length=255, blank=True, null=True)
    approved_admin_id = models.IntegerField(default='0')
    user_agree = models.CharField(max_length=5, blank=True, null=True)
    img_profile = models.ImageField(upload_to = 'img_profile/', default = 'img_profile/blank_profile.png')

class user_task(models.Model):
    id = models.AutoField(primary_key=True)
    task_id = models.CharField(max_length=255, blank=True, null=True)
    datetime_create = models.DateTimeField(default=now, editable=False, blank=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)