from django.contrib.auth import login as dj_login, logout as dj_logout
from django.shortcuts import render, redirect ,HttpResponse
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from .tokens import account_activation_token
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.conf import settings
from django.core.mail import EmailMessage
import os.path
from .forms import AdminFormSignup, LoginForm , UserFormSignup , ResetChangePasswordForm, ChangePasswordForm ,PasswordResetRequestForm

from datetime import datetime
from django.utils.translation import ugettext_lazy as _

from django.db import connection
from .models import user_profile as db_profile


# home page for user
homePath = '/home/'
dashboardPath = '/dashboard/'
# other page
projectPath = '/ManageUsers'
loginPath = '/home/'
adminPage = '/ManageUsers/AdminApproved/'



# Signup function
def UserSignup(request):
    args = {}

    if request.method == 'POST':
        form = UserFormSignup(request.POST)

        # Check register button
        if 'registerbtn' in request.POST:
            if form.is_valid():
                user = form.save()
                user = User.objects.filter(username=form.cleaned_data.get('username')).first()

                # email confirmation
                current_site = get_current_site(request)
                subject = _('Confirm registration from Metagenomic')
                message = render_to_string('acc_active_email.html', {
                    'user': user,
                    'domain': current_site,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                    'detail': 'คลิกเพื่อทำการอนุมัติ'
                })
                user.email_user(subject, message)
                message_header =_('Register successfully.')
                message_detail =_('Please confirm your email.')
                message_header_json = str(message_header)
                message_detail_json = str(message_detail)
                request.session['sm_value'] = {'message_header': message_header_json,
                                               'message_detail': message_detail_json,
                                               'type': 'success',
                                               'displaytime': '10'}
                request.session['goto'] = homePath

                return redirect(projectPath+'/status_message/')

            else:
                print(form.errors.as_data())

    else:
        # if not post show form signup
        form = UserFormSignup()

    args['form'] = form
    return render(request, 'user_signup.html', args)


# Signup function
def AdminSignup(request):
    args = {}

    if request.method == 'POST':
        form = AdminFormSignup(request.POST)

        # Check register button
        if 'registerbtn' in request.POST:
            if form.is_valid():
                user = form.save()
                user = User.objects.filter(username=form.cleaned_data.get('username')).first()

                # email confirmation
                current_site = get_current_site(request)
                subject = _('Confirm registration from Metagenomic')
                message = render_to_string('acc_active_email.html', {
                    'user': user,
                    'domain': current_site,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode,
                    'token': account_activation_token.make_token(user),
                })
                user.email_user(subject, message)
                message_header =_('Register successfully.')
                message_detail =_('Please confirm your email.')
                message_header_json = str(message_header)
                message_detail_json = str(message_detail)
                request.session['sm_value'] = {'message_header': message_header_json,
                                               'message_detail': message_detail_json,
                                               'type': 'success',
                                               'displaytime': '10'}
                request.session['goto'] = homePath

                return redirect(projectPath+'/status_message/')

            else:
                print(form.errors.as_data())

    else:
        # if not post show form signup
        form = AdminFormSignup()

    args['form'] = form
    return render(request, 'admin_signup.html', args)


#json in array for row
def dictfetchall(cursor): 
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

# Login function
def login(request):     
    args = {}
    form = LoginForm(request.POST or None)

    # check if post button login check user pass
    if request.method == 'POST':
        if 'loginBtn' in request.POST and form.is_valid():

            user = form.login(request)
            request.user.id = user.id
            user_id = request.user.id
            # check new user create storage folder
            check_create_storage(request)

            cursor = connection.cursor()
            cursor.execute("SELECT auth_group.name as group_name \
                            FROM auth_group , auth_user_groups \
                            WHERE auth_group.id = auth_user_groups.group_id \
                            AND auth_user_groups.user_id = \'"+ str(user_id) +"\'")
            row = dictfetchall(cursor)
            for i in row:
                group_name = str(i["group_name"])
            
            if user is not None:
                if group_name == 'admin':
                    dj_login(request, user)
                    if 'next' in request.POST:
                        return redirect(adminPage)
                    return redirect(adminPage)
                elif group_name == 'user':
                    dj_login(request, user)
                    if 'next' in request.POST:
                        next_to = request.GET.get('next')
                        return redirect(next_to)
                    return redirect(dashboardPath)

                
                return redirect('/ManageUser/logout')

        elif 'test' in request.POST:
            message_header =_('Register successfully.')
            message_detail =_('Please confirm your email.')
            message_header_json = str(message_header)
            message_detail_json = str(message_detail)
            request.session['sm_value'] = {'message_header': message_header_json,
                                           'message_detail': message_detail_json,
                                           'type': 'success',
                                           'displaytime': '10'}
            request.session['goto'] = homePath


            return redirect(projectPath + '/status_message/')

    args['form'] = form

    if request.user.is_authenticated:
        user_id = request.user.id
        cursor = connection.cursor()
        cursor.execute("SELECT auth_group.name as group_name \
                        FROM auth_group , auth_user_groups \
                        WHERE auth_group.id = auth_user_groups.group_id \
                        AND auth_user_groups.user_id = \'"+ str(user_id) +"\'")
        row = dictfetchall(cursor)
        for i in row:
            group_name = str(i["group_name"])

        if group_name == 'user':
            return redirect(homePath)
            # return HttpResponse('user_page')
        elif group_name == 'admin':
            return redirect(adminPage)
            # return HttpResponse('user_page')
        else:
            return redirect('/ManageUsers/logout')

    return render(request, 'login.html', args)


def activate_account(request, uidb64, token):

    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        #dj_login(request, user)
        message_header =_('Confirm email successfully.')
        message_detail =_('Enjoy using our system.')
        message_header_json = str(message_header)
        message_detail_json = str(message_detail)
        request.session['sm_value'] = {'message_header': message_header_json,
                                       'message_detail': message_detail_json,
                                       'type': 'success',
                                       'displaytime': '5'}
        request.session['goto'] = loginPath

        return redirect(projectPath+'/status_message/')
    else:
        message_header =_('Confirm email failed.')
        message_detail =_('Please contact officer.')
        message_header_json = str(message_header)
        message_detail_json = str(message_detail)
        request.session['sm_value'] = {'message_header': message_header_json,
                                       'message_detail': message_detail_json,
                                       'type': 'text-center',
                                       'displaytime': '5'}

        request.session['goto'] = homePath


        return redirect(projectPath+'/status_message/')


# Check and Create folder
def check_create_storage(request):

    try:
        user_id = request.session.get('user_id')
        path = str(user_id)
        default_path = os.path.abspath(settings.MEDIA_ROOT+"/storage/"+path)
        if not os.path.exists(settings.MEDIA_ROOT+"/storage/"+path):
            os.makedirs(default_path)
    except OSError as e:
        if e.errno == 17:
            return False


def status_message(request):
    args = {}

    try:
        args['goto'] = request.session['goto']
        sm_value = request.session['sm_value']
        args['type'] = sm_value['type']
        args['message_header'] = sm_value['message_header']
        args['message_detail'] = sm_value['message_detail']
        args['displaytime'] = sm_value['displaytime']

        del request.session['goto']
        del request.session['sm_value']

        return render(request, 'status_message.html', args)

    except KeyError:
        return redirect(homePath)


def password_reset_request(request):
    args = {}
    form = PasswordResetRequestForm(request.POST or None)
    if request.method == 'POST':
        if 'resetBtn' in request.POST and form.is_valid():
            user = User.objects.filter(email=form.cleaned_data.get('email')).first()

            # email confirmation
            current_site = get_current_site(request)
            subject = _('Confirm password change from Metagenomic')
            message = render_to_string('acc_reset_password_email.html', {
                'user': user,
                'domain': current_site,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode,
                'token': account_activation_token.make_token(user),
                'date': urlsafe_base64_encode(force_bytes(datetime.now().strftime('%y%m%d'))).decode,
                'time': urlsafe_base64_encode(force_bytes(datetime.now().strftime('%H%M'))).decode,
            })
            user.email_user(subject, message)
            message_header =_('Request a Password Change')
            message_detail =_('Please confirm the password at your email')
            message_header_json = str(message_header)
            message_detail_json = str(message_detail)

            request.session['sm_value'] = {'message_header': message_header_json,
                                           'message_detail': message_detail_json,
                                           'type': 'info',
                                           'displaytime': '10'}
            request.session['goto'] = homePath

            return redirect(projectPath+'/status_message/')

    args['form'] = form
    return render(request, 'password_reset_request.html', args)


def activate_password_reset(request, uidb64, token, date, time):
    email_expired = 30

    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
        date = force_text(urlsafe_base64_decode(date))
        time = force_text(urlsafe_base64_decode(time))
        dt = datetime.strptime(date + time, '%y%m%d%H%M')
        diff_dt = datetime.now() - dt
        #diff_dt_minute = divmod(diff_dt.days * 86400 + diff_dt.seconds, 60)
        diff_dt_minute = (diff_dt.days * 86400 + diff_dt.seconds)/60
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    # request correct
    if user is not None and account_activation_token.check_token(user, token) and diff_dt_minute <= email_expired:
        request.session['reset_password'] = {'user': uid,
                                             'dt': datetime.now().strftime('%y%m%d%H%M')}
        # dj_login(request, user)
        return redirect(projectPath+'/user/reset/password/change/')

    # request expired
    elif user is not None and account_activation_token.check_token(user, token) and diff_dt_minute > email_expired:
        message_header =_('Reset password failed.')
        message_detail =_('Reset Passwords request expired. Please login again')
        message_header_json = str(message_header)
        message_detail_json = str(message_detail)
        request.session['sm_value'] = {'message_header': message_header_json,
                                       'message_detail': message_detail_json,
                                       'type': 'error',
                                       'displaytime': '10'}
        request.session['goto'] = homePath

        return redirect(projectPath + '/status_message/')

    else:
        message_header =_('Reset password failed.')
        message_detail =_('Please contact officer.')
        message_header_json = str(message_header)
        message_detail_json = str(message_detail)
        request.session['sm_value'] = {'message_header': message_header_json,
                                       'message_detail': message_detail_json,
                                       'type': 'error',
                                       'displaytime': '10'}
        request.session['goto'] = homePath

        return redirect(projectPath+'/status_message/')


def reset_change_password_request(request):
    args = {}
    form = ResetChangePasswordForm(request.POST or None)

    # if not request.user.is_authenticated():
    #     return redirect(homePath)

    if request.method == 'POST':
        if 'resetBtn' in request.POST and form.is_valid():
            try:
                user = User.objects.get(pk=request.session['reset_user'])
                user.set_password(form.cleaned_data.get('password1'))
                user.save()
                del request.session['reset_user']
                message_header =_('Reset password successfully')
                message_detail =_('New password is saved successfully')
                message_header_json = str(message_header)
                message_detail_json = str(message_detail)
                request.session['sm_value'] = {'message_header': message_header_json,
                                               'message_detail': message_detail_json,
                                               'type': 'success',
                                               'displaytime': '10'}
                request.session['goto'] = homePath

                return redirect(projectPath + '/status_message/')

            except KeyError:
                message_header =_('Reset password failed.')
                message_detail =_('Please a new request.')
                message_header_json = str(message_header)
                message_detail_json = str(message_detail)
                request.session['sm_value'] = {'message_header': message_header_json,
                                               'message_detail': message_detail_json,
                                               'type': 'error',
                                               'displaytime': '10'}
                request.session['goto'] = homePath

                return redirect(projectPath + '/status_message/')

    else:
        try:
            rs_value = request.session['reset_password']
            user = User.objects.get(pk=rs_value['user'])
            dt = datetime.strptime(rs_value['dt'], '%y%m%d%H%M')
            diff_dt = datetime.now() - dt
            diff_dt_minute = (diff_dt.days * 86400 + diff_dt.seconds) / 60
            del request.session['reset_password']

            request.session['reset_user'] = rs_value['user']

        except KeyError:
            message_header =_('Reset password failed.')
            message_detail =_('Please a new request.')
            message_header_json = str(message_header)
            message_detail_json = str(message_detail)
            request.session['sm_value'] = {'message_header': message_header_json,
                                           'message_detail': message_detail_json,
                                           'type': 'error',
                                           'displaytime': '10'}
            request.session['goto'] = homePath

            return redirect(projectPath + '/status_message/')

    args['form'] = form
    return render(request, 'reset_change_password.html', args)


def change_password(request):
    if request.user.is_authenticated:
        user = User.objects.get(pk=request.user.id)
    else:
        return redirect(homePath)

    args = {}
    form = ChangePasswordForm(request.POST or None)

    if request.method == 'POST':
        if 'cpBtn' in request.POST and form.is_valid():
            chk_old_pass_flag = user.check_password(form.cleaned_data.get('password0'))

            if chk_old_pass_flag:
                user.set_password(form.cleaned_data.get('password1'))
                user.save()
                message_header =_('Reset password successfully')
                message_detail =_('New password is saved successfully. Please login again')
                message_header_json = str(message_header)
                message_detail_json = str(message_detail)
                request.session['sm_value'] = {'message_header': message_header_json,
                                               'message_detail': message_detail_json,
                                               'type': 'success',
                                               'displaytime': '10'}
                request.session['goto'] = homePath

                return redirect(projectPath + '/status_message/')
            else:
                message_header =_('Reset password failed.')
                message_detail =_('The old password incorrect. Please login again')
                message_header_json = str(message_header)
                message_detail_json = str(message_detail)
                request.session['sm_value'] = {'message_header': message_header_json,
                                               'message_detail': message_detail_json,
                                               'type': 'error',
                                               'displaytime': '3'}
                request.session['goto'] = projectPath + '/user/change_password/'

                return redirect(projectPath + '/status_message/')

    args['form'] = form
    return render(request, 'change_password.html', args)


def logout(request):
    try:

        dj_logout(request)
        return redirect(homePath)

    except KeyError:
        pass
        return redirect(request,loginPath)


def user_profile(request):
    uid = str(request.user.id)
    status = None
    if request.method == 'POST':
        first_name = request.POST.get('first_name',None)
        last_name = request.POST.get('last_name',None)
        nationd_id = request.POST.get('nationd_id',None)
        mobile_tel = request.POST.get('mobile_tel',None)
        organization = request.POST.get('organization',None)
        sub_organization = request.POST.get('sub_organization',None)
        department = request.POST.get('department',None)

        User.objects.update(
            first_name = first_name,
            last_name = last_name,
        )
        db_profile.objects.update(
            nationd_id = nationd_id,
            mobile_tel = mobile_tel,
            organization = organization,
            sub_organization = sub_organization,
            department = department,
        )
        status = 'Saved'
        
    user = list(User.objects.filter(id=uid).values('first_name','last_name'))[0]
    profile = list(db_profile.objects.filter(id=uid).values())[0]
    content = {
        'user2':user,
        'profile':profile,
        'status':status
    }
    return render(request , 'user_profile_page.html', content)
