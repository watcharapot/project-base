from django.conf.urls import include, url
from . import views , views_admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth import views as auth_views
app_name = 'ManageUsers'

#set name url with function in views  manage user

urlpatterns = [
    url(r'^UserSignup/$', views.UserSignup, name='UserSignup'),
    # url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^status_message', views.status_message, name='status_message'),
    url(r'^user/validate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate_account, name='activate_account'),
    url(r'^user/reset/password/request/$', views.password_reset_request,
        name='password_reset_request'),
    url(r'^user/reset/password/validate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/(?P<date>[0-9A-Za-z_\-]+)/(?P<time>[0-9A-Za-z_\-]+)/$',
        views.activate_password_reset, name='activate_password_reset'),
    url(r'^user/reset/password/change/$', views.reset_change_password_request,
        name='reset_change_password'),
    url(r'^user/change_password/$', views.change_password,
        name='change_password'),
    url(r'^ProfileSetting/$', views.user_profile, name='ProfileSetting'),
    url(r'^AdminApproved/$', views_admin.admin_approved_user, name='AdminApproved'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()