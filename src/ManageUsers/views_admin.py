from __future__ import unicode_literals
from django.http import HttpResponse ,Http404
from django.template import loader
from django.db import connection
from django.template.loader import get_template
from django.template import Context
import subprocess
import shlex
from django.shortcuts import render, redirect ,HttpResponseRedirect
from django.contrib.auth.decorators import login_required
import base64
from functools import wraps
from django.contrib.auth.models import User ,Group
from django.core.mail import EmailMessage
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.shortcuts import get_current_site
from ManageUsers.models import user_profile

def check_groups(group_name):
    def _check_group(view_func):
        @wraps(view_func)
        def wrapper(request, *args, **kwargs):
            # print(request.user.id)
            if request.user.is_anonymous:
                return redirect('/admin/')
            if (not (request.user.groups.filter(name=group_name).exists())
                or request.user.is_superuser):
                # raise(Http404)
                return render(None,'404_page.html')
            return view_func(request, *args, **kwargs)
        return wrapper
    return _check_group


@login_required(login_url="/home/")
@check_groups('admin')
def admin_approved_user(request):
    # list all user
    users = User.objects.all().filter(is_active=0)
    my_user_id = str(request.user.id)
    
    # check name button approved
    if 'approved' in request.POST:
        approved_id = request.POST.getlist('checkAll[]')
        for pk_id in approved_id:
            active = User.objects.get(id=pk_id)
            # my email for admin
            my_email = active.email
            # approved
            active.is_active = True
            user_profile.objects.filter(user_id=pk_id).update(approved_admin_id=my_user_id)
            # send email
            current_site = get_current_site(request)
            subject = _('Confirm registration from ' + str(current_site))
            message = _('Account your approved')
            email = EmailMessage(subject, message, to=[my_email])
            active.save()
            email.send()

    # check name button reject
    if 'reject' in request.POST:
        reject_id = request.POST.getlist('checkAll[]')
        for pk_id in reject_id:
            # reject and delete user
            user_profile.objects.filter(user_id=pk_id).delete()
            cursor = connection.cursor()
            cursor.execute("DELETE FROM auth_user_groups WHERE user_id = \'"+ str(pk_id) +"\'")
            User.objects.filter(id=pk_id).delete()

    content = {
        'users':users
    }
    return render(request , 'admin_approved_page.html' , content)