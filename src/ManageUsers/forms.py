from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from ManageUsers.models import user_profile
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from django.db import connection
import datetime

# Create Signup form in django form


class AdminFormSignup(UserCreationForm):


    username = forms.CharField(required=True, label=_(''), widget=forms.TextInput(
        attrs={'placeholder': _('Username'), 'type': 'hidden'}))
    first_name = forms.CharField(required=True, label=_('FirstName'), widget=forms.TextInput(
        attrs={'placeholder': _('FirstName'), 'class': 'form-control'}))
    last_name = forms.CharField(required=True, label=_('Lastname'), widget=forms.TextInput(
        attrs={'placeholder': _('Lastname'), 'class': 'form-control'}))
    email = forms.EmailField(required=True, label=_('Email'), widget=forms.TextInput(
        attrs={'placeholder': _('Email'), 'oninput': 'myFunction()', 'class': 'form-control'}))
    password1 = forms.CharField(required=True, label=_('Password'),
                                widget=forms.TextInput(attrs={'placeholder': _('Password'), 'type': 'password', 'class': 'form-control'}))
    password2 = forms.CharField(required=True, label=_('Confirm Password'),
                                widget=forms.TextInput(attrs={'placeholder': _('Confirm Password'), 'type': 'password', 'class': 'form-control'}))
    
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name',
                  'email', 'password1', 'password2')

    class meta:
        model = user_profile
        fields = ('user_id','run_limit','file_limit','file_size_gb','approved_admin_id','img_profile')

    def clean_email(self):
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')

        if email and User.objects.filter(email=email).exclude(username=username).count():
            raise forms.ValidationError(_('This email is already used'))
        return email

    def save(self, commit=True):
        user = super(AdminFormSignup, self).save(commit=False)

        if commit:
            user.is_active = False
            user.save()
            # insert auth_user_group
            cursor = connection.cursor()
            cursor.execute(
                "INSERT INTO auth_user_groups (user_id,group_id) VALUES ( \'" + str(user.id) + "\' ,'1')")
            
            profile = user_profile(user_id = user.id)
            profile.save()
            
            
# ////////////////////////////////////////////////////////////////////////////////
# ////////////////////////////////////////////////////////////////////////////////

class UserFormSignup(UserCreationForm):

    username = forms.CharField(required=True, label=_('UserName'), widget=forms.TextInput(
        attrs={'placeholder': _(''), 'type': 'text' ,'class':'form-control'}))
    first_name = forms.CharField(required=True, label=_('FirstName'), widget=forms.TextInput(
        attrs={'placeholder': _(''), 'class': 'form-control'}))
    last_name = forms.CharField(required=True, label=_('Lastname'), widget=forms.TextInput(
        attrs={'placeholder': _(''), 'class': 'form-control'}))
    email = forms.EmailField(required=True, label=_('Email'), widget=forms.TextInput(
        attrs={'placeholder': _(''), 'oninput': 'cp_username()', 'class': 'form-control'}))
    password1 = forms.CharField(required=True, label=_('Password'),
                                widget=forms.TextInput(attrs={'placeholder': _(''), 'type': 'password', 'class': 'form-control'}))
    password2 = forms.CharField(required=True, label=_('Confirm Password'),
                                widget=forms.TextInput(attrs={'placeholder': _(''), 'type': 'password', 'class': 'form-control'}))
    national_id = forms.CharField(required=True, label=_('National ID'), widget=forms.TextInput(
        attrs={'placeholder': _(''), 'class': 'form-control'}))
    mobile_tel = forms.CharField(required=True, label=_('Mobile Tel'), widget=forms.TextInput(
        attrs={'placeholder': _(''), 'class': 'form-control'}))
    organization = forms.CharField(required=True, label=_('Organization'), widget=forms.TextInput(
        attrs={'placeholder': _(''), 'class': 'form-control'}))
    sub_organization = forms.CharField(required=True, label=_('Sub Organization'), widget=forms.TextInput(
        attrs={'placeholder': _(''), 'class': 'form-control'}))
    department = forms.CharField(required=True, label=_('Department'), widget=forms.TextInput(
        attrs={'placeholder': _(''), 'class': 'form-control'}))
        
    
    
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name',
                  'email', 'password1', 'password2')

    class meta:
        model = user_profile
        fields = ('user_id','approved_admin_id','img_profile','user_agree','national_id','mobile_tel','organization','sub_organization','department')

    def clean_email(self):
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')

        if email and User.objects.filter(email=email).exclude(username=username).count():
            raise forms.ValidationError(_('This email is already used'))
        return email

    def save(self, commit=True):
        user = super(UserFormSignup, self).save(commit=False)

        if commit:
            user.is_active = False
            user.save()
            # insert auth_user_group
            cursor = connection.cursor()
            cursor.execute(
                "INSERT INTO auth_user_groups (user_id,group_id) VALUES ( \'" + str(user.id) + "\' ,'2')")

            national_id = self.cleaned_data.get('national_id')
            mobile_tel = self.cleaned_data.get('mobile_tel')
            organization = self.cleaned_data.get('organization')
            sub_organization = self.cleaned_data.get('sub_organization')
            department = self.cleaned_data.get('department')
            profile = user_profile(
                user_id = user.id,
                user_agree='agree',
                nationd_id=national_id,
                mobile_tel=mobile_tel,
                organization=organization,
                sub_organization=sub_organization,
                department=department,
            )
            profile.save()

# ////////////////////////////////////////////////////////////////////////////////
# ////////////////////////////////////////////////////////////////////////////////

# Create Login form in django form


class LoginForm(forms.Form):
    username = forms.CharField(required=True,
                               widget=forms.TextInput(attrs={'placeholder': _('Email address'),'class':'form-control'}))
    password = forms.CharField(required=True, 
                               widget=forms.TextInput(attrs={'placeholder': _('Password'), 'type': 'password','class':'form-control'}))

    def __int__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            if not user.is_active:
                raise forms.ValidationError(_("Please wait admin confirm."))
        else:
            raise forms.ValidationError(_("Invalid username or password."))

        return self.cleaned_data

    def login(self, request):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        return user


class PasswordResetRequestForm(forms.Form):
    email = forms.EmailField(required=True, label='Email', widget=forms.TextInput(
        attrs={'placeholder': _('Email'),'class':'form-control'}))

    def clean_email(self):
        email = self.cleaned_data.get('email')

        if email and User.objects.filter(email=email).count() == 0:
            raise forms.ValidationError(_('Invalid email'))
        return email


class ResetChangePasswordForm(forms.Form):
    password1 = forms.CharField(required=True, label='Password',
                                widget=forms.TextInput(attrs={'placeholder': _('Password'), 'type': 'password','class':'form-control'}))
    password2 = forms.CharField(required=True, label='Confirm Password',
                                widget=forms.TextInput(attrs={'placeholder': _('Confirm Password'), 'type': 'password','class':'form-control'}))

    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 != password2:
            raise forms.ValidationError(_("The password didn't match"))

        return self.cleaned_data


class ChangePasswordForm(forms.Form):
    password0 = forms.CharField(required=True, label='Old Password',
                                widget=forms.TextInput(attrs={'placeholder': _('Old Password'), 'type': 'password','class':'form-control'}))
    password1 = forms.CharField(required=True, label='New Password',
                                widget=forms.TextInput(attrs={'placeholder': _('New Password'), 'type': 'password','class':'form-control'}))
    password2 = forms.CharField(required=True, label='Confirm Password',
                                widget=forms.TextInput(attrs={'placeholder': _('Confirm Password'), 'type': 'password','class':'form-control'}))

    def __int__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 != password2:
            raise forms.ValidationError(_("The password didn't match"))

        return self.cleaned_data


