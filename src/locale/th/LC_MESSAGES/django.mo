��    `        �         (  %   )     O     U     ^     b  
   o  
   z     �     �     �      �     �     �     �  !   	     8	     W	  
   _	     j	  	   y	     �	     �	     �	  
   �	  	   �	     �	     �	  R   �	     2
     8
     F
     d
  	   m
     w
     }
     �
     �
     �
  �   �
     S     `  "   l  6   �  4   �     �       
        %     1  
   :     E  )   [     �     �     �     �     �     �     �  
   �               $     2     @     M     d  3   ~     �     �     �     �     �               1     @     Q     _     h     v     ~     �     �     �     �  .   �     �               "     +     2     6  
   >  d  I  =   �     �  $        -  *   =  !   h     �     �  -   �  H   �  r   -  *   �  K   �  H     b   `  P   �          '  $   C     h     �     �  T   �  !   �       !   7  !   Y  �   {       -   &  �   T     �     �  '        .  '   M  $   u  4   �  }  �  $   M  $   r  ~   �  �     �   �  $   �  !   �  !   �            -   0  N   ^  u   �  �   #  B   �  ?        F     \  3   r  9   �  !   �  '     !   *  '   L  0   t  !   �  -   �  <   �  �   2   W   �   T   +!     �!  !   �!  <   �!  $   �!  0   "     B"  9   ^"  !   �"     �"     �"     �"  *   #  $   ?#  !   d#  !   �#  9   �#  �   �#  ?   z$  6   �$     �$     %     #%     >%     N%  '   g%     X       C       7       R                 Y   8       (   M              
                [   S          @       ;   B       N   	   W   $      1   F       /      .   K   U             O          \               I      ,   +   <   Q   G   #   E                 =   *   D   H   6   A           `   %           P       Z              '   T      ?   L   )       "   5   4   J         9   &      V             -         _          0      ^   !   2         3       ]       >       :                113 Khlong Luang PathumThani,Thailand About About Us Add All Projects All Sample Call us on Cancel Change password Click link to comfirm Click link to confirm a password Confirm Password Confirm email failed. Confirm email successfully. Confirm password change from GWAS Confirm registration from GWAS Contact Contact Us Create Project Dashboard Document Email Enjoy using our system. Find us at FirstName Forget Password Forget password Gwas framework is the section in the form of a website used for DNA data analysis. Hello Invalid email Invalid username or password. Lastname Locations Login Logout Manage projects Manage user Mon-Sat 9am-5pm (GMT) National Center for Genetic Engineering and Biotechnology (BIOTEC) 113 Thailand Science Park Phahonyothin Road Khlong Nueng, Khlong Luang Pathum Thani 12120 New Password New Project New password is saved successfully New password is saved successfully. Please login again Note: The 30 minute expired then make a new request. Old Password Other Information Owner name Owner share Password Permission Please a new request. Please confirm the password at your email Please confirm your email. Please contact officer. Privacy Policy Profile Project Project Detail Project Directory Project ID Project Information Project Name Project Title Project info. Project name Register successfully. Request a Password Change Reset Passwords request expired. Please login again Reset password failed. Reset password successfully Sample Select image: Select permission : Select project Select sample path: Select samples Set new password Share Project Share to Share with me Sign up Site Map Social Submit System message Terms and Conditions The old password incorrect. Please login again The password didn't match This email is already used User Username action etc only me other user Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-16 15:26+0700
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 113 อ.คลองหลวง จ.ปทุมธานี เกี่ยวกับ เกี่ยวกับเรา เพิ่ม โครงงานทั้งหมด ไฟล์ทั้งหมด โทรหาเรา ยกเลิก เปลี่ยนรหัสผ่าน คลิกที่ลิ้งค์เพื่อยืนยัน คลิกที่ลิ้งค์เพื่อยืนยันการเปลี่ยนรหัส ยืนยันรหัสผ่าน ยืนยันการลงทะเบียนล้มเหลว ยืนยันการลงทะเบียนสำเร็จ ยืนยันการเปลี่ยนรหัสผ่านจากระบบ GWAS ยืนยันการลงทะเบียนจากระบบ GWAS ติดต่อ ติดต่อเรา สร้างโครงงาน แผงควบคุม เอกสาร อีเมล ขอให้สนุกกับการใช้ระบบของเรา หาเราได้ที่ ชื่อจริง ลืมรหัสผ่าน ลืมรหัสผ่าน Gwas framework เป็นรูปแบบเว็บไซต์ที่ใช้ในการวิเคราะห์ข้อมูล DNA สวัสดี อีเมลไม่ถูกต้อง ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง กรุณาลองใหม่ นามสกุล ที่ตั้ง ลงชื่อเข้าใช้ ออกจากระบบ จัดการโครงงาน จัดการผู้ใช้ จันทร์ - เสาร์ 9.00-17.00 น. ศูนย์พันธุวิศวกรรมและเทคโนโลยีชีวภาพแห่งชาติ 113 อุทยานวิทยาศาสตร์ประเทศไทย ถนนพหลโยธิน ตำบลคลองหนึ่ง อำเภอคลองหลวง จังหวัดปทุมธานี 12120 รหัสผ่านใหม่ สร้างโครงงาน รหัสผ่านผู้ใช้ใหม่ถูกบันทึกในระบบเรียบร้อย รหัสผ่านผู้ใช้ใหม่ถูกบันทึกในระบบเรียบร้อย กรุณาลงชื่อเข้าระบบอีกครั้ง หมายเหตุ : คำร้องมีอายุ 30 นาที หลังจากน้ันจะต้องทำการร้องขอใหม่ รหัสผ่านเดิม ข้อมูลอื่นๆ ชื่อเจ้าของ เจ้าของ รหัสผ่าน สิทธิ์การใช้งาน กรุณาส่งคำร้องใหม่อีกครั้ง โปรดยืนยันการเปลี่ยนรหัสที่อีเมลของท่าน กรุณายืนยันการลงทะเบียนที่อีเมลของท่านก่อนเริ่มใช้งาน กรุณาติดต่อเจ้าหน้าที่ นโยบายความเป็นส่วนตัว ประวัติ โครงงาน รายละเอียดโครงงาน ไดเรกทอรีของโครงงาน รหัสโครงงาน ข้อมูลโครงงาน ชื่อโครงงาน หัวข้อโครงงาน ข้อมูลของโครงงาน ชื่อโครงงาน ลงทะเบียนสำเร็จ คำร้องการเปลี่ยนรหัส คำร้องรีเซตรหัสผ่านหมดอายุ กรุณายื่นคำร้องใหม่อีกครั้ง การตั้งค่ารหัสผ่านใหม่ผิดพลาด การตั้งค่ารหัสผ่านใหม่สำเร็จ ไฟล์ เลือกรูปภาพ เลือกสิทธิ์การใช้งาน เลือกโครงงาน เลือกที่เก็บไฟล์ เลือกไฟล์ ตั้งค่ารหัสผ่านใหม่ แชร์โครงงาน แชร์ให้กับ แชร์กับฉัน ลงทะเบียน แผนผังเว็บไซต์ สังคมออนไลน์ ส่งแบบฟอร์ม ข้อความระบบ เงื่อนไขและข้อกำหนด กำหนดค่ารหัสผ่านเดิมไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง รหัสผ่านใหม่ไม่ตรงกัน อีเมลนี้ถูกใช้แล้ว ผู้ใช้ ชื่อผู้ใช้ การกระทำ** อื่นๆ เฉพาะฉัน ผู้ใช้รายอื่น 