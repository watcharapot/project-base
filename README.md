# README #



### install python 3.7 or anaconda###


download
https://www.anaconda.com/products/individual

Install library pip
project-base/src/requirements.txt
```sh
$ pip install -r requirements.txt
```

### .env setting  ###
project-base/src/.env
```sh
MYSQL_DATABASE_NAME="base_project"
MYSQL_DATABASE_USER="user"
MYSQL_DATABASE_PASSWORD="passpword"
MYSQL_DATABASE_HOST="127.0.0.1"
MYSQL_DATABASE_PORT=3306
```

### deploy database mariadb  ###

```sh
$ docker run --name some-mariadb -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mariadb:latest
```

### django cmd ###

```sh
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py loaddata group.json  #import groups
$ python manage.py runserver 0:8000
```


### push image ###

```sh
$ docker push nbtnew/hcv-project:tagname
```